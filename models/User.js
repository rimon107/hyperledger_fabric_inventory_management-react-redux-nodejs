const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  affiliation: {
    type: String,
    required: true
  },
  role: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  avatar: {
    type: String
  },
  initiator: {
    type: String,
    required: false
  },
  currentState: {
    type: String,
    required: false
  },
  txId: {
    type: String,
    required: false
  },
  channelId: {
    type: String,
    required: false
  },
  dateTime: {
    type: String,
    required: false
  },
  model: {
    type: String
  }
});

module.exports = User = mongoose.model("users", UserSchema);
