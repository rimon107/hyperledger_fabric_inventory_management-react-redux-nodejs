const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const ItemSchema = new Schema({
  type: {
    type: String,
    required: true
  },
  itemId: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  quantity: {
    type: String,
    required: true
  },
  initiator: {
    type: String,
    required: false
  },
  currentState: {
    type: String,
    required: false
  },
  txId: {
    type: String,
    required: false
  },
  channelId: {
    type: String,
    required: false
  },
  dateTime: {
    type: String,
    required: false
  },
  model: {
    type: String
  }
});

module.exports = Item = mongoose.model("items", ItemSchema);
