const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const UserRequestSchema = new Schema({
  id: {
    type: String,
    required: true
  },
  user: {
    type: String,
    required: true
  },
  item: {
    type: String,
    required: true
  },
  quantity: {
    type: String,
    required: true
  },
  state: {
    type: String,
    required: true
  },
  initiator: {
    type: String,
    required: false
  },
  currentState: {
    type: String,
    required: false
  },
  txId: {
    type: String,
    required: false
  },
  channelId: {
    type: String,
    required: false
  },
  dateTime: {
    type: String,
    required: false
  },
  model: {
    type: String
  }
});

module.exports = UserRequest = mongoose.model(
  "userRequests",
  UserRequestSchema
);
