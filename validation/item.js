const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateItemInput(data) {
  let errors = {};

  data.type = !isEmpty(data.type) ? data.type : "";
  data.itemId = !isEmpty(data.itemId) ? data.itemId : "";
  data.name = !isEmpty(data.name) ? data.name : "";
  data.description = !isEmpty(data.description) ? data.description : "";
  data.quantity = !isEmpty(data.quantity) ? data.quantity : "";

  if (Validator.isEmpty(data.type)) {
    errors.type = "Item Type field is required";
  }

  if (Validator.isEmpty(data.itemId)) {
    errors.itemId = "item Number field is required";
  }

  if (Validator.isEmpty(data.name)) {
    errors.name = "Item name field is required";
  }

  if (Validator.isEmpty(data.description)) {
    errors.description = "Item description field is required";
  }

  if (Validator.isEmpty(data.quantity)) {
    errors.quantity = "Item quantity field is required";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
