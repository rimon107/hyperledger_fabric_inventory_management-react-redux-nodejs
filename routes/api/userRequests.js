const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const passport = require("passport");
const yaml = require("js-yaml");
// const path = require("path");

const { FileSystemWallet, Gateway } = require("fabric-network");
const fs = require("fs");
const path = require("path");

// Load connection profile; will be used to locate a gateway
const connectionProfile = yaml.safeLoad(
  fs.readFileSync(path.resolve("../../basic-network/connection.yaml"), "utf8")
);

// User Request model
const UserRequest = require("../../models/UserRequest");

// @route   GET api/items/test
// @desc    Tests paper route
// @access  Public
router.get("/test", (req, res) => res.json({ msg: "item Works" }));

async function createRequest(request) {
  try {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    // let userName = item.initiator;
    let userName = "user1";

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        'An identity for the user "user1" does not exist in the wallet'
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Create a new gateway for connecting to our peer node.
    // const gateway = new Gateway();
    // await gateway.connect(ccp, { wallet, identity: 'user1', discovery: { enabled: false } });

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    // Submit the specified transaction.
    // createCar transaction - requires 5 argument, ex: ('createCar', 'CAR12', 'Honda', 'Accord', 'Black', 'Tom')
    // changeCarOwner transaction - requires 2 args , ex: ('changeCarOwner', 'CAR10', 'Dave')
    await contract.submitTransaction(
      "createRequest",
      request.user,
      request.item,
      request.quantity,
      request.state,
      request.initiator,
      request.currentState,
      request.model
    );
    console.log("Transaction has been submitted");

    var result = await contract.evaluateTransaction(
      "queryRequestByModel",
      request.model
    );

    var bytes = JSON.parse(result);

    // Disconnect from the gateway.
    await gateway.disconnect();

    return bytes;
  } catch (error) {
    console.error(`Failed to submit transaction: ${error}`);
    process.exit(1);
  }
}

async function updateRequest(request) {
  try {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    // let userName = item.initiator;
    let userName = "user1";

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        'An identity for the user "user1" does not exist in the wallet'
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Create a new gateway for connecting to our peer node.
    // const gateway = new Gateway();
    // await gateway.connect(ccp, { wallet, identity: 'user1', discovery: { enabled: false } });

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    // Submit the specified transaction.
    // createCar transaction - requires 5 argument, ex: ('createCar', 'CAR12', 'Honda', 'Accord', 'Black', 'Tom')
    // changeCarOwner transaction - requires 2 args , ex: ('changeCarOwner', 'CAR10', 'Dave')

    if (request.initiator === "store") {
      console.log("request.id");
      console.log(request.id);

      console.log("request.quantity");
      console.log(request.quantity);

      console.log("request");
      console.log(request);

      let result_item = await contract.evaluateTransaction(
        "queryAllItemsByName",
        request.item
      );

      var bytes_item = JSON.parse(result_item);

      console.log("bytesitem");
      console.log(bytes_item);

      const quant =
        parseInt(bytes_item[0].Record.quantity) - parseInt(request.quantity);

      console.log("quant");
      console.log(quant);

      if (parseInt(quant) < 0) {
        return;
      }

      await contract.submitTransaction(
        "updateExistingItem",
        bytes_item[0].Key,
        quant.toString(),
        "update",
        request.initiator,
        "item"
      );
      console.log("Item transaction has been submitted");
    }

    await contract.submitTransaction(
      "updateExistingRequest",
      request.id,
      request.state,
      request.initiator,
      request.currentState
    );
    console.log("Request transaction has been submitted");

    var result = await contract.evaluateTransaction(
      "queryRequestById",
      request.id
    );

    var bytes = JSON.parse(result);

    // Disconnect from the gateway.
    await gateway.disconnect();

    return bytes;
  } catch (error) {
    console.error(`Failed to submit transaction: ${error}`);
    process.exit(1);
  }
}

async function getRequestById(requestId) {
  try {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    const userName = "user1";

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        'An identity for the user "user1" does not exist in the wallet'
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    var result = await contract.evaluateTransaction(
      "queryRequestById",
      requestId
    );

    // var arrayOfBytes = JSON.parse(result)[0].TransactionDetails.data;

    // var json = JSON.parse(new Buffer(arrayOfBytes).toString());

    // var x = JSON.parse(JSON.parse(result)[0].TransactionDetails).Initiator
    //     .id_bytes.buffer;

    // console.log(new Buffer(x).toString("ascii"));

    // console.log("JSON.parse(result)");
    // console.log(JSON.parse(result));

    var bytes = JSON.parse(result);

    // console.log(JSON.parse(result));

    // var result = await contract.evaluateTransaction('queryItemById',
    // '8cff8dc4bd0bfbbcefbcec2ace1f78f3de1e59762a1c643e5d2f81e418e272f8');

    // // var bytes = JSON.parse(result.toString()).Initiator.id_bytes.buffer.data;

    // console.log(JSON.parse(result));

    return bytes;
  } catch (error) {
    console.error(`Failed to evaluate transaction: ${error}`);
    process.exit(1);
  }
}

async function getRequestHistory(requestId) {
  try {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    // const userName = username;
    let userName = item.initiator;

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        `An identity for the user "${userName}" does not exist in the wallet`
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    var result = await contract.evaluateTransaction(
      "requestHistory",
      requestId
    );

    // var arrayOfBytes = JSON.parse(result)[0].TransactionDetails.data;

    // var json = JSON.parse(new Buffer(arrayOfBytes).toString());

    // var x = JSON.parse(JSON.parse(result)[0].TransactionDetails).Initiator
    //     .id_bytes.buffer;

    // console.log(new Buffer(x).toString("ascii"));

    console.log("JSON.parse(result)");
    console.log(JSON.parse(result));

    var bytes = JSON.parse(result);

    // console.log(JSON.parse(result));

    // var result = await contract.evaluateTransaction('queryItemById',
    // '8cff8dc4bd0bfbbcefbcec2ace1f78f3de1e59762a1c643e5d2f81e418e272f8');

    // // var bytes = JSON.parse(result.toString()).Initiator.id_bytes.buffer.data;

    // console.log(JSON.parse(result));

    return bytes;
  } catch (error) {
    console.error(`Failed to evaluate transaction: ${error}`);
    process.exit(1);
  }
}

async function getRequestByState(state) {
  try {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    const userName = "user1";

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        'An identity for the user "user1" does not exist in the wallet'
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    var result = await contract.evaluateTransaction(
      "queryRequestByState",
      state
    );

    // var arrayOfBytes = JSON.parse(result)[0].TransactionDetails.data;

    // var json = JSON.parse(new Buffer(arrayOfBytes).toString());

    // var x = JSON.parse(JSON.parse(result)[0].TransactionDetails).Initiator
    //     .id_bytes.buffer;

    // console.log(new Buffer(x).toString("ascii"));

    // console.log("JSON.parse(result)");
    // console.log(JSON.parse(result));

    var bytes = JSON.parse(result);

    // console.log(JSON.parse(result));

    // var result = await contract.evaluateTransaction('queryItemById',
    // '8cff8dc4bd0bfbbcefbcec2ace1f78f3de1e59762a1c643e5d2f81e418e272f8');

    // // var bytes = JSON.parse(result.toString()).Initiator.id_bytes.buffer.data;

    // console.log(JSON.parse(result));

    return bytes;
  } catch (error) {
    console.error(`Failed to evaluate transaction: ${error}`);
    process.exit(1);
  }
}

async function getRequestByUser(user) {
  try {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    const userName = "user1";

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        'An identity for the user "user1" does not exist in the wallet'
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    var result = await contract.evaluateTransaction("queryRequestByUser", user);

    // var arrayOfBytes = JSON.parse(result)[0].TransactionDetails.data;

    // var json = JSON.parse(new Buffer(arrayOfBytes).toString());

    // var x = JSON.parse(JSON.parse(result)[0].TransactionDetails).Initiator
    //     .id_bytes.buffer;

    // console.log(new Buffer(x).toString("ascii"));

    // console.log("JSON.parse(result)");
    // console.log(JSON.parse(result));

    var bytes = JSON.parse(result);

    // console.log(JSON.parse(result));

    // var result = await contract.evaluateTransaction('queryItemById',
    // '8cff8dc4bd0bfbbcefbcec2ace1f78f3de1e59762a1c643e5d2f81e418e272f8');

    // // var bytes = JSON.parse(result.toString()).Initiator.id_bytes.buffer.data;

    // console.log(JSON.parse(result));

    return bytes;
  } catch (error) {
    console.error(`Failed to evaluate transaction: ${error}`);
    process.exit(1);
  }
}

async function getRequestByModel(model) {
  try {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    const userName = "user1";

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        'An identity for the user "user1" does not exist in the wallet'
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    var result = await contract.evaluateTransaction(
      "queryRequestByModel",
      model
    );

    // var arrayOfBytes = JSON.parse(result)[0].TransactionDetails.data;

    // var json = JSON.parse(new Buffer(arrayOfBytes).toString());

    // var x = JSON.parse(JSON.parse(result)[0].TransactionDetails).Initiator
    //     .id_bytes.buffer;

    // console.log(new Buffer(x).toString("ascii"));

    // console.log("JSON.parse(result)");
    // console.log(JSON.parse(result));

    var bytes = JSON.parse(result);

    console.log(bytes);

    // var result = await contract.evaluateTransaction('queryItemById',
    // '8cff8dc4bd0bfbbcefbcec2ace1f78f3de1e59762a1c643e5d2f81e418e272f8');

    // // var bytes = JSON.parse(result.toString()).Initiator.id_bytes.buffer.data;

    // console.log(JSON.parse(result));

    return bytes;
  } catch (error) {
    console.error(`Failed to evaluate transaction: ${error}`);
    process.exit(1);
  }
}

router.get(
  "/all",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    let result = [];
    let items;
    // let store_item;

    console.log("req.user.name");
    console.log(req.user.name);

    const role = req.user.name;

    if (role === "superadmin") {
      items = await getRequestByState("request");
    } else if (role === "store") {
      items = await getRequestByState("forwarded");
      // store_item = await getRequestByState("processed");
    } else if (role === "officer") {
      items = await getRequestByModel("UserRequest");
    } else {
      items = await getRequestByModel("UserRequest");
    }

    // console.log("items");
    // console.log(items);
    items.forEach(makeResult);
    // store_item.forEach(makeResult);
    function makeResult(value, index) {
      let newItem = new UserRequest({
        id: value.Record.id,
        user: value.Record.user,
        item: value.Record.item,
        quantity: value.Record.quantity,
        state: value.Record.state,
        initiator: value.Record.initiator,
        currentState: value.Record.currentState,
        dateTime: value.Record.dateTime,
        channelId: value.Record.channelId,
        txId: value.Record.txId
      });

      result.push(newItem);
    }
    function makeResult(value, index) {
      let newItem = new UserRequest({
        id: value.Record.id,
        user: value.Record.user,
        item: value.Record.item,
        quantity: value.Record.quantity,
        state: value.Record.state,
        initiator: value.Record.initiator,
        currentState: value.Record.currentState,
        dateTime: value.Record.dateTime,
        channelId: value.Record.channelId,
        txId: value.Record.txId
      });

      result.push(newItem);
    }
    // console.log("result");
    // console.log(result);
    res.json(result);
  }
);

router.post(
  "/request",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    // const { errors, isValid } = validateItemInput(req.body);

    // Check Validation
    // if (!isValid) {
    //   // If any errors, send 400 with errors object
    //   return res.status(400).json(errors);
    // }

    const newReq = new UserRequest({
      user: req.user.name,
      item: req.body.item,
      quantity: req.body.quantity,
      state: "request",
      initiator: req.user.name,
      currentState: "create",
      dateTime: req.body.dateTime,
      model: "UserRequest"
    });

    createRequest(newReq)
      .then(item => {
        console.log(item);
        console.log("Request create program  complete.");
        res.json(item);
      })
      .catch(e => {
        console.log("Request create program exception.");
        console.log(e);
        console.log(e.stack);
        process.exit(-1);
      });
  }
);

router.post(
  "/request/update",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    // const { errors, isValid } = validateItemInput(req.body);

    // // Check Validation
    // if (!isValid) {
    //   // If any errors, send 400 with errors object
    //   return res.status(400).json(errors);
    // }

    const newReq = new Item({
      quantity: req.body.quantity,
      state: req.body.state,
      initiator: req.user.name,
      currentState: "updated by " + req.user.name,
      dateTime: req.body.dateTime,
      model: "userRequest"
    });

    updateRequest(newReq)
      .then(item => {
        console.log("Request update program complete.");
        res.json(item);
      })
      .catch(e => {
        console.log("Request create program exception.");
        console.log(e);
        console.log(e.stack);
        process.exit(-1);
      });
  }
);

router.get("/request/:id", async (req, res) => {
  const errors = {};

  console.log(req.params.id);

  let value = await getRequestById(req.params.id);

  console.log(value);

  let newReq = new UserRequest({
    id: value.id,
    user: value.user,
    item: value.item,
    quantity: value.quantity,
    state: value.state,
    currentState: value.currentState,
    dateTime: value.dateTime,
    channelId: value.channelId,
    txId: value.txId
  });

  res.json(newReq);
});

router.post(
  "/request/history/:id",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    let result = [];

    let items = await getRequestHistory(req.params.id);
    console.log(items);

    items.forEach(makeResult);
    function makeResult(value, index) {
      let newItem = new UserRequest({
        id: value.Record.id,
        user: value.Record.user,
        item: value.Record.item,
        quantity: value.Record.quantity,
        state: value.Record.state,
        initiator: value.Record.initiator,
        currentState: value.Record.currentState,
        dateTime: value.Record.dateTime,
        channelId: value.Record.channelId,
        txId: value.Record.txId
      });

      console.log("result");
      console.log(result);
      result.push(newItem);
    }
    res.json(result);
  }
);

router.post(
  "/request/forward/:id",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    let value = await getRequestById(req.params.id);

    console.log(value);

    let valReq = new UserRequest({
      id: value.id,
      user: value.user,
      item: value.item,
      quantity: value.quantity,
      state: value.state,
      currentState: value.currentState,
      dateTime: value.dateTime,
      channelId: value.channelId,
      txId: value.txId
    });

    const newReq = new UserRequest({
      id: value.id,
      quantity: valReq.quantity,
      state: "forwarded",
      initiator: req.user.name,
      currentState: "updated by " + req.user.name,
      dateTime: req.body.dateTime,
      model: "UserRequest"
    });

    updateRequest(newReq)
      .then(item => {
        console.log("Request update program complete.");
        res.json(item);
      })
      .catch(e => {
        console.log("Request create program exception.");
        console.log(e);
        console.log(e.stack);
        process.exit(-1);
      });
  }
);

router.post(
  "/request/cancel/:id",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    console.log("cancel");
    console.log(req.params.id);
    let value = await getRequestById(req.params.id);

    console.log("cancel");
    console.log(value);

    let valReq = new UserRequest({
      id: value.id,
      user: value.user,
      item: value.item,
      quantity: value.quantity,
      state: value.state,
      currentState: value.currentState,
      dateTime: value.dateTime,
      channelId: value.channelId,
      txId: value.txId
    });

    const newReq = new UserRequest({
      id: value.id,
      quantity: valReq.quantity,
      state: "cancel",
      initiator: req.user.name,
      currentState: "updated by " + req.user.name,
      dateTime: req.body.dateTime,
      model: "UserRequest"
    });

    updateRequest(newReq)
      .then(item => {
        console.log("Request update program complete.");
        res.json(item);
      })
      .catch(e => {
        console.log("Request create program exception.");
        console.log(e);
        console.log(e.stack);
        process.exit(-1);
      });
  }
);

router.post(
  "/request/processed/:id",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    console.log("processed");
    console.log(req.params.id);
    let value = await getRequestById(req.params.id);

    console.log("processed");
    console.log(value);

    let valReq = new UserRequest({
      id: value.id,
      user: value.user,
      item: value.item,
      quantity: value.quantity,
      state: value.state,
      currentState: value.currentState,
      dateTime: value.dateTime,
      channelId: value.channelId,
      txId: value.txId
    });

    const newReq = new UserRequest({
      id: valReq.id,
      user: value.user,
      item: value.item,
      quantity: valReq.quantity,
      state: "processed",
      initiator: req.user.name,
      currentState: "updated by " + req.user.name,
      dateTime: req.body.dateTime,
      model: "UserRequest"
    });

    updateRequest(newReq)
      .then(item => {
        console.log("Request update program complete.");
        res.json(item);
      })
      .catch(e => {
        console.log("Request create program exception.");
        console.log(e);
        console.log(e.stack);
        process.exit(-1);
      });
  }
);

module.exports = router;
