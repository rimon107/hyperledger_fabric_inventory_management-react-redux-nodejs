const express = require("express");
const router = express.Router();
const gravatar = require("gravatar");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
const passport = require("passport");

const yaml = require("js-yaml");
const FabricCAServices = require("fabric-ca-client");
const {
  FileSystemWallet,
  X509WalletMixin,
  Gateway
} = require("fabric-network");
const fs = require("fs");
const path = require("path");

// Load connection profile; will be used to locate a gateway
const connectionProfile = yaml.safeLoad(
  fs.readFileSync(path.resolve("../../basic-network/connection.yaml"), "utf8")
);

// Load Input Validation
const validateRegisterInput = require("../../validation/register");
const validateUserCreateInput = require("../../validation/user");
const validateLoginInput = require("../../validation/login");

// Load User model
const User = require("../../models/User");

async function createUser(user) {
  try {
    // // Create a new file system based wallet for managing identities.
    // const walletPath = path.join(process.cwd(), "wallet");
    // const wallet = new FileSystemWallet(walletPath);
    // // console.log(`Wallet path: ${walletPath}`);

    // let userName = user.name;

    // console.log(user);

    // // Check to see if we've already enrolled the user.
    // const userExists = await wallet.exists(userName);
    // if (userExists) {
    //   console.log(
    //     `An identity for the user "${userName}" already exists in the wallet`
    //   );
    //   return;
    // }

    // // Check to see if we've already enrolled the admin user.
    // const adminExists = await wallet.exists(user.initiator);
    // if (!adminExists) {
    //   console.log(
    //     `An identity for the admin user "${user.initiator}" does not exist in the wallet`
    //   );
    //   console.log("Only Admin can create user");
    //   return;
    // }

    // // Create a new gateway for connecting to our peer node.
    // // const gateway = new Gateway();
    // // await gateway.connect(ccp, { wallet, identity: 'user1', discovery: { enabled: false } });

    // // Set connection options; identity and wallet
    // let connectionOptions = {
    //   identity: user.initiator,
    //   wallet: wallet,
    //   discovery: { enabled: false, asLocalhost: true }
    // };

    // // Create a new gateway for connecting to our peer node.
    // const gateway = new Gateway();
    // // await gateway.connect(ccpPath, {
    // //   wallet,
    // //   identity: userName,
    // //   discovery: { enabled: true, asLocalhost: true }
    // // });

    // await gateway.connect(connectionProfile, connectionOptions);

    // // Get the CA client object from the gateway for interacting with the CA.
    // const ca = gateway.getClient().getCertificateAuthority();
    // const adminIdentity = gateway.getCurrentIdentity();

    // // Register the user, enroll the user, and import the new identity into the wallet.
    // // const secret = await ca.register({ affiliation: 'org1.department1', enrollmentID: user.email, role: user.role }, adminIdentity);
    // const secret = await ca.register(
    //   {
    //     affiliation: user.affiliation,
    //     enrollmentID: user.name,
    //     role: user.role
    //   },
    //   adminIdentity
    // );
    // const enrollment = await ca.enroll({
    //   enrollmentID: user.name,
    //   enrollmentSecret: secret
    // });
    // const userIdentity = X509WalletMixin.createIdentity(
    //   "Org1MSP",
    //   enrollment.certificate,
    //   enrollment.key.toBytes()
    // );
    // wallet.import(user.name, userIdentity);

    // console.log(
    //   `Successfully registered and enrolled user "${user.name}" and imported it into the wallet`
    // );

    // // Get the network (channel) our contract is deployed to.
    // const network = await gateway.getNetwork("mychannel");

    // // Get the contract from the network.
    // const contract = network.getContract("fabcar");

    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    // let userName = item.initiator;
    let userName = "user1";

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        'An identity for the user "user1" does not exist in the wallet'
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Create a new gateway for connecting to our peer node.
    // const gateway = new Gateway();
    // await gateway.connect(ccp, { wallet, identity: 'user1', discovery: { enabled: false } });

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(user.password, salt, (err, hash) => {
        if (err) throw err;
        user.password = hash;
      });
    });

    await contract.submitTransaction(
      "createUser",
      user.name,
      user.affiliation,
      user.role,
      user.email,
      user.password,
      user.currentState,
      user.initiator,
      user.model
    );

    console.log("User create transaction has been submitted");

    var result = await contract.evaluateTransaction(
      "queryUserByEmail",
      user.email
    );

    console.log("queryUserByEmail transaction has been submitted");

    var bytes = JSON.parse(result);

    // Disconnect from the gateway.
    await gateway.disconnect();

    return bytes;
  } catch (error) {
    console.error(`Failed to register user : ${error}`);
    process.exit(1);
  }
}

async function createAdmin(user) {
  try {
    // // Create a new CA client for interacting with the CA.
    // // const caURL = ccp.certificateAuthorities["ca.example.com"].url;
    // const caURL = "http://localhost:7054";
    // const ca = new FabricCAServices(caURL);

    // // Create a new file system based wallet for managing identities.
    // const walletPath = path.join(process.cwd(), "wallet");
    // const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    // let userEmail = user.email;

    // // Check to see if we've already enrolled the user.
    // const userExists = await wallet.exists(userEmail);
    // if (userExists) {
    //   console.log(
    //     `An identity for the admin user "${userEmail}" already exists in the wallet`
    //   );
    //   return;
    // }

    // // Enroll the admin user, and import the new identity into the wallet.
    // const enrollment = await ca.enroll({
    //   enrollmentID: user.email,
    //   enrollmentSecret: user.password
    // });
    // const identity = X509WalletMixin.createIdentity(
    //   "Org1MSP",
    //   enrollment.certificate,
    //   enrollment.key.toBytes()
    // );
    // wallet.import(user.email, identity);
    // console.log(
    //   'Successfully enrolled admin user "admin" and imported it into the wallet'
    // );

    // // Create a new gateway for connecting to our peer node.
    // // const gateway = new Gateway();
    // // await gateway.connect(ccp, { wallet, identity: 'user1', discovery: { enabled: false } });

    // // Set connection options; identity and wallet
    // let connectionOptions = {
    //   identity: userName,
    //   wallet: wallet,
    //   discovery: { enabled: false, asLocalhost: true }
    // };

    // // Create a new gateway for connecting to our peer node.
    // const gateway = new Gateway();
    // // await gateway.connect(ccpPath, {
    // //   wallet,
    // //   identity: userName,
    // //   discovery: { enabled: true, asLocalhost: true }
    // // });

    // await gateway.connect(connectionProfile, connectionOptions);

    // console.log(
    //   `Successfully registered and enrolled admin user "${user.name}" and imported it into the wallet`
    // );

    // // Get the network (channel) our contract is deployed to.
    // const network = await gateway.getNetwork("mychannel");

    // // Get the contract from the network.
    // const contract = network.getContract("fabcar");

    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    // let userName = item.initiator;
    let userName = "user1";

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        'An identity for the user "user1" does not exist in the wallet'
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Create a new gateway for connecting to our peer node.
    // const gateway = new Gateway();
    // await gateway.connect(ccp, { wallet, identity: 'user1', discovery: { enabled: false } });

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(user.password, salt, (err, hash) => {
        if (err) throw err;
        user.password = hash;
      });
    });

    // Submit the specified transaction.
    // createCar transaction - requires 5 argument, ex: ('createCar', 'CAR12', 'Honda', 'Accord', 'Black', 'Tom')
    // changeCarOwner transaction - requires 2 args , ex: ('changeCarOwner', 'CAR10', 'Dave')
    await contract.submitTransaction(
      "createUser",
      user.name,
      user.role,
      user.affiliation,
      user.password,
      user.email,
      user.avatar,
      user.initiator,
      user.currentState,
      user.model
    );

    console.log("User create transaction has been submitted");

    var result = await contract.evaluateTransaction(
      "queryUserByEmail",
      user.email
    );

    var bytes = JSON.parse(result);

    // Disconnect from the gateway.
    await gateway.disconnect();

    return bytes;
  } catch (error) {
    console.error(`Failed to register user "user1": ${error}`);
    process.exit(1);
  }
}

async function loginUser(email) {
  try {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    const userName = "user1";

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        'An identity for the user "user1" does not exist in the wallet'
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    console.log("email");
    console.log(email);

    var result = await contract.evaluateTransaction("queryUserByEmail", email);

    var bytes = JSON.parse(result);

    // Disconnect from the gateway.
    await gateway.disconnect();

    return bytes;
  } catch (error) {
    console.error(`Failed to register user "user1": ${error}`);
    process.exit(1);
  }
}

async function updateUser(user) {
  try {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    let userName = "user1";

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        'An identity for the user "user1" does not exist in the wallet'
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Create a new gateway for connecting to our peer node.
    // const gateway = new Gateway();
    // await gateway.connect(ccp, { wallet, identity: 'user1', discovery: { enabled: false } });

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    // Submit the specified transaction.
    // createCar transaction - requires 5 argument, ex: ('createCar', 'CAR12', 'Honda', 'Accord', 'Black', 'Tom')
    // changeCarOwner transaction - requires 2 args , ex: ('changeCarOwner', 'CAR10', 'Dave')
    await contract.submitTransaction(
      "updateExistingUser",
      user.name,
      user.affiliation,
      user.role,
      user.email,
      user.password,
      user.currentState,
      user.initiator
    );
    console.log("Transaction has been submitted");

    var result = await contract.evaluateTransaction(
      "queryUserByEmail",
      user.email
    );

    var bytes = JSON.parse(result);

    // Disconnect from the gateway.
    await gateway.disconnect();

    return bytes;
  } catch (error) {
    console.error(`Failed to submit transaction: ${error}`);
    process.exit(1);
  }
}

async function getAllUsers(username) {
  try {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    const userName = "user1";

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        'An identity for the user "user1" does not exist in the wallet'
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    var result = await contract.evaluateTransaction("queryAllUsers", "user");

    // var arrayOfBytes = JSON.parse(result)[0].TransactionDetails.data;

    // var json = JSON.parse(new Buffer(arrayOfBytes).toString());

    // var x = JSON.parse(JSON.parse(result)[0].TransactionDetails).Initiator
    //     .id_bytes.buffer;

    // console.log(new Buffer(x).toString("ascii"));

    // console.log("JSON.parse(result)");
    // console.log(JSON.parse(result));

    var bytes = JSON.parse(result);

    // console.log(JSON.parse(result));

    // var result = await contract.evaluateTransaction('queryItemById',
    // '8cff8dc4bd0bfbbcefbcec2ace1f78f3de1e59762a1c643e5d2f81e418e272f8');

    // // var bytes = JSON.parse(result.toString()).Initiator.id_bytes.buffer.data;

    // console.log(JSON.parse(result));

    return bytes;
  } catch (error) {
    console.error(`Failed to evaluate transaction: ${error}`);
    process.exit(1);
  }
}

async function getUserByEmail(email) {
  try {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    const userName = "user1";

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        `An identity for the user user "${userExists}" does not exist in the wallet`
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    var result = await contract.evaluateTransaction("queryUserByEmail", email);

    // var arrayOfBytes = JSON.parse(result)[0].TransactionDetails.data;

    // var json = JSON.parse(new Buffer(arrayOfBytes).toString());

    // var x = JSON.parse(JSON.parse(result)[0].TransactionDetails).Initiator
    //     .id_bytes.buffer;

    // console.log(new Buffer(x).toString("ascii"));

    // console.log("JSON.parse(result)");
    // console.log(JSON.parse(result));

    var bytes = JSON.parse(result);

    // console.log(JSON.parse(result));

    // var result = await contract.evaluateTransaction('queryItemById',
    // '8cff8dc4bd0bfbbcefbcec2ace1f78f3de1e59762a1c643e5d2f81e418e272f8');

    // // var bytes = JSON.parse(result.toString()).Initiator.id_bytes.buffer.data;

    // console.log(JSON.parse(result));

    return bytes;
  } catch (error) {
    console.error(`Failed to evaluate transaction: ${error}`);
    process.exit(1);
  }
}

async function getUserHistory(user) {
  try {
    const name = user.name;
    const email = user.email;

    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    const userName = "user1";

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        `An identity for the user "${userName}" does not exist in the wallet`
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    var result = await contract.evaluateTransaction("userHistory", email);

    // var arrayOfBytes = JSON.parse(result)[0].TransactionDetails.data;

    // var json = JSON.parse(new Buffer(arrayOfBytes).toString());

    // var x = JSON.parse(JSON.parse(result)[0].TransactionDetails).Initiator
    //     .id_bytes.buffer;

    // console.log(new Buffer(x).toString("ascii"));

    console.log("JSON.parse(result)");
    console.log(JSON.parse(result));

    var bytes = JSON.parse(result);

    // console.log(JSON.parse(result));

    // var result = await contract.evaluateTransaction('queryItemById',
    // '8cff8dc4bd0bfbbcefbcec2ace1f78f3de1e59762a1c643e5d2f81e418e272f8');

    // // var bytes = JSON.parse(result.toString()).Initiator.id_bytes.buffer.data;

    // console.log(JSON.parse(result));

    return bytes;
  } catch (error) {
    console.error(`Failed to evaluate transaction: ${error}`);
    process.exit(1);
  }
}

// @route   GET api/users/test
// @desc    Tests users route
// @access  Public
router.get("/test", (req, res) => res.json({ msg: "Users Works" }));

// @route   POST api/users/register
// @desc    Register user
// @access  Public
router.post(
  "/user/register",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateUserCreateInput(req.body);

    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors);
    }

    const avatar = gravatar.url(req.body.email, {
      s: "200", // Size
      r: "pg", // Rating
      d: "mm" // Default
    });

    const newUser = new User({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
      affiliation: req.body.affiliation,
      role: req.body.role,
      initiator: req.user.name,
      currentState: "create",
      model: "user"
    });

    if (newUser.role == "admin") {
      createAdmin(newUser)
        .then(user => res.json(user))
        .catch(err => console.log(err));
    } else {
      createUser(newUser)
        .then(user => res.json(user))
        .catch(err => console.log(err));
    }
  }
);

router.post("/user/update", (req, res) => {
  const { errors, isValid } = validateUserCreateInput(req.body);

  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const avatar = gravatar.url(req.body.email, {
    s: "200", // Size
    r: "pg", // Rating
    d: "mm" // Default
  });

  const newUser = new User({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
    affiliation: req.body.affiliation,
    role: req.body.role,
    initiator: req.user.name,
    currentState: "update"
  });

  updateUser(newUser)
    .then(user => res.json(user))
    .catch(err => console.log(err));
});

router.post(
  "/user/delete/:id",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    // const { errors, isValid } = validateUserCreateInput(req.body);

    // Check Validation
    // if (!isValid) {
    //   return res.status(400).json(errors);
    // }

    let value = await getUserByEmail(req.params.id);

    const avatar = gravatar.url(req.body.email, {
      s: "200", // Size
      r: "pg", // Rating
      d: "mm" // Default
    });

    const newUser = new User({
      name: value.name,
      email: value.email,
      password: value.password,
      affiliation: value.affiliation,
      role: value.role,
      initiator: req.params.id,
      currentState: "delete"
    });

    updateUser(newUser)
      .then(user => res.json(user))
      .catch(err => console.log(err));
  }
);

// @route   GET api/users/login
// @desc    Login User / Returning JWT Token
// @access  Public
router.post("/login", async (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body);

  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;

  loginUser(email).then(user => {
    // Check for user

    console.log("user");
    console.log(user);
    if (!user) {
      errors.email = "User not found";
      return res.status(404).json(errors);
    }

    // Check Password
    if (user.password === password) {
      // User Matched

      const payload = {
        id: user.id,
        name: user.name,
        email: user.email,
        role: user.role,
        affiliation: user.affiliation
      }; // Create JWT Payload

      console.log(payload);

      // Sign Token
      jwt.sign(payload, keys.secretOrKey, { expiresIn: 3600 }, (err, token) => {
        console.log("token");
        console.log(token);
        res.json({
          success: true,
          token: "bearer " + token
        });
      });
    } else {
      errors.password = "Password incorrect";
      return res.status(400).json(errors);
    }
  });
});

// @route   GET api/users/current
// @desc    Return current user
// @access  Private
router.get(
  "/current",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    res.json({
      id: req.user.id,
      name: req.user.name,
      email: req.user.email,
      role: req.user.role,
      affiliation: req.user.affiliation
    });
  }
);

// @route   GET api/users/all
// @desc    Get all items
// @access  Public

router.get(
  "/all",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    let result = [];

    // console.log(req.user);
    let items = await getAllUsers(req.user.name);
    // console.log("items");
    // console.log(items);
    items.forEach(makeResult);
    function makeResult(value, index) {
      let newItem = new User({
        name: value.Record.name,
        email: value.Record.email,
        password: value.Record.password,
        affiliation: value.Record.affiliation,
        role: value.Record.role,
        initiator: value.Record.initiator,
        currentState: value.Record.currentState,
        dateTime: value.Record.dateTime,
        channelId: value.Record.channelId,
        txId: value.Record.txId
      });

      result.push(newItem);
    }
    // console.log("result");
    // console.log(result);
    res.json(result);
  }
);

router.post("/user/history/:id", async (req, res) => {
  console.log("req.params.id: " + req.params.id);
  let result = [];

  const user = await getUserByEmail(req.params.id);

  let users = await getUserHistory(user);

  users.forEach(makeResult);
  function makeResult(value, index) {
    let newItem = new User({
      name: value.Record.name,
      email: req.params.id,
      password: value.Record.password,
      affiliation: value.Record.affiliation,
      role: value.Record.role,
      initiator: value.Record.initiator,
      currentState: value.Record.currentState,
      dateTime: value.Record.dateTime,
      channelId: value.Record.channelId,
      txId: value.Record.txId
    });

    // console.log("result");
    // console.log(result);
    result.push(newItem);
  }
  res.json(result);
});

module.exports = router;
