const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const passport = require("passport");
const yaml = require("js-yaml");
// const path = require("path");

const { FileSystemWallet, Gateway } = require("fabric-network");
const fs = require("fs");
const path = require("path");

// Load connection profile; will be used to locate a gateway
const connectionProfile = yaml.safeLoad(
  fs.readFileSync(path.resolve("../../basic-network/connection.yaml"), "utf8")
);

// // Item model
const Items = require("../../models/item");
// Profile model
const Profile = require("../../models/Profile");

// Validation
const validateItemInput = require("../../validation/item");

// @route   GET api/items/test
// @desc    Tests paper route
// @access  Public
router.get("/test", (req, res) => res.json({ msg: "item Works" }));

async function getAllItems(username) {
  try {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    const userName = "user1";

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        'An identity for the user "user1" does not exist in the wallet'
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    var result = await contract.evaluateTransaction("queryAllItems", "item");

    // var arrayOfBytes = JSON.parse(result)[0].TransactionDetails.data;

    // var json = JSON.parse(new Buffer(arrayOfBytes).toString());

    // var x = JSON.parse(JSON.parse(result)[0].TransactionDetails).Initiator
    //     .id_bytes.buffer;

    // console.log(new Buffer(x).toString("ascii"));

    // console.log("JSON.parse(result)");
    // console.log(JSON.parse(result));

    var bytes = JSON.parse(result);

    // console.log(JSON.parse(result));

    // var result = await contract.evaluateTransaction('queryItemById',
    // '8cff8dc4bd0bfbbcefbcec2ace1f78f3de1e59762a1c643e5d2f81e418e272f8');

    // // var bytes = JSON.parse(result.toString()).Initiator.id_bytes.buffer.data;

    // console.log(JSON.parse(result));

    return bytes;
  } catch (error) {
    console.error(`Failed to evaluate transaction: ${error}`);
    process.exit(1);
  }
}

async function createItem(item) {
  try {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    // let userName = item.initiator;
    let userName = "user1";

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        'An identity for the user "user1" does not exist in the wallet'
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Create a new gateway for connecting to our peer node.
    // const gateway = new Gateway();
    // await gateway.connect(ccp, { wallet, identity: 'user1', discovery: { enabled: false } });

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    // Submit the specified transaction.
    // createCar transaction - requires 5 argument, ex: ('createCar', 'CAR12', 'Honda', 'Accord', 'Black', 'Tom')
    // changeCarOwner transaction - requires 2 args , ex: ('changeCarOwner', 'CAR10', 'Dave')
    await contract.submitTransaction(
      "createItem",
      item.type,
      item.itemId,
      item.name,
      item.description,
      item.quantity,
      item.currentState,
      item.initiator,
      item.model
    );
    console.log("Transaction has been submitted");

    var result = await contract.evaluateTransaction(
      "queryItemByItemId",
      item.itemId
    );

    var bytes = JSON.parse(result);

    // Disconnect from the gateway.
    await gateway.disconnect();

    return bytes;
  } catch (error) {
    console.error(`Failed to submit transaction: ${error}`);
    process.exit(1);
  }
}

async function updateItem(item) {
  try {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    // let userName = item.initiator;
    let userName = "user1";

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        'An identity for the user "user1" does not exist in the wallet'
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Create a new gateway for connecting to our peer node.
    // const gateway = new Gateway();
    // await gateway.connect(ccp, { wallet, identity: 'user1', discovery: { enabled: false } });

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    // Submit the specified transaction.
    // createCar transaction - requires 5 argument, ex: ('createCar', 'CAR12', 'Honda', 'Accord', 'Black', 'Tom')
    // changeCarOwner transaction - requires 2 args , ex: ('changeCarOwner', 'CAR10', 'Dave')
    await contract.submitTransaction(
      "updateExistingItem",
      item.itemId,
      item.quantity,
      item.currentState,
      item.initiator,
      item.model
    );
    console.log("Transaction has been submitted");

    var result = await contract.evaluateTransaction(
      "queryItemByItemId",
      item.itemId
    );

    var bytes = JSON.parse(result);

    // Disconnect from the gateway.
    await gateway.disconnect();

    return bytes;
  } catch (error) {
    console.error(`Failed to submit transaction: ${error}`);
    process.exit(1);
  }
}

async function getItemById(itemId, username) {
  try {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    const userName = "user1";

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        'An identity for the user "user1" does not exist in the wallet'
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    var result = await contract.evaluateTransaction(
      "queryItemByItemId",
      itemId
    );

    // var arrayOfBytes = JSON.parse(result)[0].TransactionDetails.data;

    // var json = JSON.parse(new Buffer(arrayOfBytes).toString());

    // var x = JSON.parse(JSON.parse(result)[0].TransactionDetails).Initiator
    //     .id_bytes.buffer;

    // console.log(new Buffer(x).toString("ascii"));

    // console.log("JSON.parse(result)");
    // console.log(JSON.parse(result));

    var bytes = JSON.parse(result);

    // console.log(JSON.parse(result));

    // var result = await contract.evaluateTransaction('queryItemById',
    // '8cff8dc4bd0bfbbcefbcec2ace1f78f3de1e59762a1c643e5d2f81e418e272f8');

    // // var bytes = JSON.parse(result.toString()).Initiator.id_bytes.buffer.data;

    // console.log(JSON.parse(result));

    return bytes;
  } catch (error) {
    console.error(`Failed to evaluate transaction: ${error}`);
    process.exit(1);
  }
}

async function getItemHistory(itemId, username) {
  try {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.join(process.cwd(), "wallet");
    const wallet = new FileSystemWallet(walletPath);
    // console.log(`Wallet path: ${walletPath}`);

    // const userName = username;
    let userName = item.initiator;

    // Check to see if we've already enrolled the user.
    const userExists = await wallet.exists(userName);
    if (!userExists) {
      console.log(
        `An identity for the user "${userName}" does not exist in the wallet`
      );
      console.log("Run the registerUser.js application before retrying");
      return;
    }

    // Set connection options; identity and wallet
    let connectionOptions = {
      identity: userName,
      wallet: wallet,
      discovery: { enabled: false, asLocalhost: true }
    };

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    // await gateway.connect(ccpPath, {
    //   wallet,
    //   identity: userName,
    //   discovery: { enabled: true, asLocalhost: true }
    // });

    await gateway.connect(connectionProfile, connectionOptions);

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork("mychannel");

    // Get the contract from the network.
    const contract = network.getContract("fabcar");

    var result = await contract.evaluateTransaction("itemHistory", itemId);

    // var arrayOfBytes = JSON.parse(result)[0].TransactionDetails.data;

    // var json = JSON.parse(new Buffer(arrayOfBytes).toString());

    // var x = JSON.parse(JSON.parse(result)[0].TransactionDetails).Initiator
    //     .id_bytes.buffer;

    // console.log(new Buffer(x).toString("ascii"));

    console.log("JSON.parse(result)");
    console.log(JSON.parse(result));

    var bytes = JSON.parse(result);

    // console.log(JSON.parse(result));

    // var result = await contract.evaluateTransaction('queryItemById',
    // '8cff8dc4bd0bfbbcefbcec2ace1f78f3de1e59762a1c643e5d2f81e418e272f8');

    // // var bytes = JSON.parse(result.toString()).Initiator.id_bytes.buffer.data;

    // console.log(JSON.parse(result));

    return bytes;
  } catch (error) {
    console.error(`Failed to evaluate transaction: ${error}`);
    process.exit(1);
  }
}

// @route   GET api/items/all
// @desc    Get all items
// @access  Public
router.get(
  "/all",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    let result = [];

    console.log("req.user.name");
    console.log(req.user.name);
    let items = await getAllItems(req.user.name);
    // console.log("items");
    // console.log(items);
    items.forEach(makeResult);
    function makeResult(value, index) {
      let newItem = new Item({
        type: value.Record.type,
        itemId: value.Key,
        name: value.Record.name,
        description: value.Record.description,
        quantity: value.Record.quantity,
        initiator: value.Record.initiator,
        currentState: value.Record.currentState,
        dateTime: value.Record.dateTime,
        channelId: value.Record.channelId,
        txId: value.Record.txId
      });

      result.push(newItem);
    }
    // console.log("result");
    // console.log(result);
    res.json(result);
  }
);

// @route   GET api/item/:id
// @desc    Get item from id
// @access  Public
router.get("/item/:id", async (req, res) => {
  const errors = {};

  console.log(req.params.id);

  let value = await getItemById(req.params.id, "user1");

  console.log(value);

  let newItem = new Item({
    type: value.type,
    itemId: req.params.id,
    name: value.name,
    description: value.description,
    quantity: value.quantity,
    initiator: value.initiator,
    currentState: value.currentState,
    dateTime: value.dateTime,
    channelId: value.channelId,
    txId: value.txId
  });

  res.json(newItem);
});

// @route   POST api/item
// @desc    Create item
// @access  Private
router.post(
  "/item",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateItemInput(req.body);

    // Check Validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }
    const newItem = new Item({
      type: req.body.type,
      itemId: req.body.itemId,
      name: req.body.name,
      description: req.body.description,
      quantity: req.body.quantity,
      initiator: req.user.name,
      currentState: "create",
      dateTime: req.body.dateTime,
      model: "item"
    });

    createItem(newItem)
      .then(item => {
        console.log("Item create program  complete.");
        res.json(item);
      })
      .catch(e => {
        console.log("Item create program exception.");
        console.log(e);
        console.log(e.stack);
        process.exit(-1);
      });
  }
);

// @route   POST api/item
// @desc    Update item
// @access  Private
router.post(
  "/item/update",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateItemInput(req.body);

    // Check Validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }
    const newItem = new Item({
      type: req.body.type,
      itemId: req.body.itemId,
      name: req.body.name,
      description: req.body.description,
      quantity: req.body.quantity,
      initiator: req.user.name,
      currentState: "update",
      dateTime: req.body.dateTime,
      model: "item"
    });

    // newItem.save().then(item => res.json(item));

    updateItem(newItem)
      .then(item => {
        console.log("Item update program complete.");
        res.json(item);
      })
      .catch(e => {
        console.log("Item create program exception.");
        console.log(e);
        console.log(e.stack);
        process.exit(-1);
      });
  }
);

// @route   POST api/item
// @desc    Delete item
// @access  Private
router.post(
  "/item/delete/:id",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const errors = {};

    // let it;
    // Items.findOne({ _id: req.params.id })
    //   .then(itemFound => {
    //     if (!itemFound) {
    //       errors.noitem = "There is no item for this id";
    //       res.status(404).json(errors);
    //     }

    let value = await getItemById(req.params.id, req.user.name);

    let itemFound = new Item({
      type: value.type,
      itemId: req.params.id,
      name: value.name,
      description: value.description,
      quantity: value.quantity,
      initiator: value.initiator,
      currentState: "delete",
      dateTime: value.dateTime,
      channelId: value.channelId,
      txId: value.txId,
      model: value.model
    });

    updateItem(itemFound)
      .then(item => {
        console.log("Item delete program complete.");
        res.json(item);
      })
      .catch(e => {
        console.log("Item delete program exception.");
        console.log(e);
        console.log(e.stack);
        process.exit(-1);
      });
  }
);

// @route   POST api/item
// @desc    History item
// @access  Private
router.post(
  "/item/history/:id",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    let result = [];

    console.log(req.params.id);
    console.log(req.params);

    let items = await getItemHistory(req.params.id, req.user.name);
    console.log(items);

    items.forEach(makeResult);
    function makeResult(value, index) {
      let newItem = new Item({
        type: value.Record.type,
        itemId: req.params.id,
        name: value.Record.name,
        description: value.Record.description,
        quantity: value.Record.quantity,
        initiator: value.Record.initiator,
        currentState: value.Record.currentState,
        dateTime: value.Record.dateTime,
        channelId: value.Record.channelId,
        txId: value.Record.txId
      });

      console.log("result");
      console.log(result);
      result.push(newItem);
    }
    res.json(result);
  }
);

module.exports = router;
