/*
 * SPDX-License-Identifier: Apache-2.0
 */

"use strict";

const { Contract } = require("fabric-contract-api");
// const bcrypt = require("bcryptjs");

class FabCar extends Contract {
    async initLedger(ctx) {
        console.info("============= START : Initialize Ledger ===========");

        const txId = await ctx.stub.getTxID();
        const channelId = await ctx.stub.getChannelID();
        const timeStamp = new Date(
            (await ctx.stub.getTxTimestamp().seconds.low) * 1000
        ).toString();

        let password = "asdf1234";

        // bcrypt.genSalt(10, (err, salt) => {
        //     bcrypt.hash(password, salt, (err, hash) => {
        //         if (err) throw err;
        //         password = hash;
        //     });
        // });

        const users = [
            {
                id: "5e06e42ed7bb3629c6daff3c",
                name: "superadmin",
                affiliation: "org1.department1",
                role: "superadmin",
                email: "superadmin@org1.com",
                password: password,
                currentState: "create",
                initiator: "superadmin",
                txId: txId,
                channelId: channelId,
                dateTime: timeStamp,
                model: "user"
            },
            {
                id: "5e06e42ed7bb3629c6daff3b",
                name: "admin",
                affiliation: "org1.department1",
                role: "admin",
                email: "admin@org1.com",
                password: password,
                currentState: "create",
                initiator: "superadmin",
                txId: txId,
                channelId: channelId,
                dateTime: timeStamp,
                model: "user"
            },
            {
                id: "5e06e42ed7bb3629c6daff4b",
                name: "officer",
                affiliation: "org1.department1",
                role: "officer",
                email: "officer@org1.com",
                password: password,
                currentState: "create",
                initiator: "superadmin",
                txId: txId,
                channelId: channelId,
                dateTime: timeStamp,
                model: "user"
            },
            {
                id: "5e06e42ed5bb3629c6daff3b",
                name: "store",
                affiliation: "org1.department1",
                role: "store",
                email: "store@org1.com",
                password: password,
                currentState: "create",
                initiator: "superadmin",
                txId: txId,
                channelId: channelId,
                dateTime: timeStamp,
                model: "user"
            }
        ];

        const items = [
            {
                type: "pen",
                name: "mat-01",
                description: "mat-01",
                quantity: "10",
                currentState: "create",
                initiator: "admin",
                txId: txId,
                channelId: channelId,
                dateTime: timeStamp,
                model: "item"
            },
            {
                type: "pen",
                name: "mat-02",
                description: "mat-02",
                quantity: "14",
                currentState: "create",
                initiator: "admin",
                txId: txId,
                channelId: channelId,
                dateTime: timeStamp,
                model: "item"
            }
        ];

        const req = [
            {
                id: txId,
                user: "officer",
                item: "mat-01",
                quantity: "10",
                state: "request",
                currentState: "create",
                initiator: "officer",
                txId: txId,
                channelId: channelId,
                dateTime: timeStamp,
                model: "UserRequest"
            }
        ];

        for (let i = 0; i < items.length; i++) {
            await ctx.stub.putState(
                items[i].type + "-" + i,
                Buffer.from(JSON.stringify(items[i]))
            );
            console.info("Added <--> ", items[i]);
        }

        for (let i = 0; i < users.length; i++) {
            await ctx.stub.putState(
                users[i].email,
                Buffer.from(JSON.stringify(users[i]))
            );
            console.info("Added <--> ", users[i]);
        }

        for (let i = 0; i < req.length; i++) {
            await ctx.stub.putState(
                req[i].id,
                Buffer.from(JSON.stringify(req[i]))
            );
            console.info("Added <--> ", req[i]);
        }
        console.info("============= END : Initialize Ledger ===========");
    }

    // Inventory modification
    async createItem(
        ctx,
        type,
        itemId,
        name,
        description,
        quantity,
        currentState,
        initiator,
        model
    ) {
        console.info("============= START : Create Item ===========");

        const txId = await ctx.stub.getTxID();
        const channelId = await ctx.stub.getChannelID();
        const dateTime = new Date(
            (await ctx.stub.getTxTimestamp().seconds.low) * 1000
        ).toString();

        const item = {
            type,
            itemId,
            name,
            description,
            quantity,
            currentState,
            initiator,
            txId,
            channelId,
            dateTime,
            model
        };

        // this.storeTransaction(ctx);
        await ctx.stub.putState(itemId, Buffer.from(JSON.stringify(item)));
        console.info("============= END : Create Item ===========");
    }

    async updateExistingItem(
        ctx,
        itemId,
        newQuantity,
        status,
        initiator,
        model
    ) {
        console.info("============= START : Update Item ===========");

        const txId = await ctx.stub.getTxID();
        const channelId = await ctx.stub.getChannelID();
        const timeStamp = new Date(
            (await ctx.stub.getTxTimestamp().seconds.low) * 1000
        ).toString();

        var queryString = `{"selector":{"itemId":"${itemId}"}}`;
        const iterator = await ctx.stub.getQueryResult(queryString);
        const allResults = [];

        const qitem = await ctx.stub.getState(itemId); // get the car from chaincode state
        if (!qitem || qitem.length === 0) {
            throw new Error(`${itemId} does not exist`);
        }

        const item = JSON.parse(qitem.toString());

        item.quantity = newQuantity.toString();
        item.currentState = status;
        item.initiator = initiator;
        item.txId = txId;
        item.channelId = channelId;
        item.dateTime = timeStamp;
        item.model = model;

        await ctx.stub.putState(itemId, Buffer.from(JSON.stringify(item)));

        console.info("============= END : Update Item ===========");
    }

    async queryAllItemsByType(ctx, type) {
        var queryString = `{"selector":{"type":"${type}"}}`;
        const iterator = await ctx.stub.getQueryResult(queryString);

        // const iterator = await ctx.stub.getStateByRange(startKey, endKey);

        const allResults = [];
        while (true) {
            const res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString("utf8"));

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString("utf8"));
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString("utf8");
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log("end of data");
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

    async queryAllItemsByName(ctx, name) {
        var queryString = `{"selector":{"name":"${name}"}}`;
        const iterator = await ctx.stub.getQueryResult(queryString);

        // const iterator = await ctx.stub.getStateByRange(startKey, endKey);

        const allResults = [];
        while (true) {
            const res = await iterator.next();

            console.log(res);

            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString("utf8"));

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString("utf8"));
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString("utf8");
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log("end of data");
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

    async queryAllItemsByInitiator(ctx, initiator) {
        var queryString = `{"selector":{"initiator":"${initiator}"}}`;
        const iterator = await ctx.stub.getQueryResult(queryString);

        // const iterator = await ctx.stub.getStateByRange(startKey, endKey);

        const allResults = [];
        while (true) {
            const res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString("utf8"));

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString("utf8"));
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString("utf8");
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log("end of data");
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

    async queryItemByItemId(ctx, itemId) {
        const itemAsBytes = await ctx.stub.getState(itemId); // get the item from chaincode state
        if (!itemAsBytes || itemAsBytes.length === 0) {
            throw new Error(`${itemNumber} does not exist`);
        }
        console.log(itemAsBytes.toString());
        return itemAsBytes.toString();
    }

    async itemHistory(ctx, itemNumber) {
        const iterator = await ctx.stub.getHistoryForKey(itemNumber);
        return this.getAllResults(ctx, iterator);
    }

    async queryAllItems(ctx, model) {
        var queryString = `{"selector":{"model":"${model}"}}`;
        const iterator = await ctx.stub.getQueryResult(queryString);

        // const iterator = await ctx.stub.getStateByRange(startKey, endKey);

        const allResults = [];
        while (true) {
            const res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString("utf8"));

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString("utf8"));
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString("utf8");
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log("end of data");
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

    //User Management
    async createUser(
        ctx,
        name,
        affiliation,
        role,
        email,
        password,
        currentState,
        initiator,
        model
    ) {
        console.info("============= START : Create Item ===========");

        const txId = await ctx.stub.getTxID();
        const channelId = await ctx.stub.getChannelID();
        const dateTime = new Date(
            (await ctx.stub.getTxTimestamp().seconds.low) * 1000
        ).toString();

        const user = {
            name,
            affiliation,
            role,
            email,
            password,
            currentState,
            initiator,
            txId,
            channelId,
            dateTime,
            model
        };

        // this.storeTransaction(ctx);
        await ctx.stub.putState(email, Buffer.from(JSON.stringify(user)));
        console.info("============= END : Create Item ===========");
    }

    async updateExistingUser(
        ctx,
        name,
        affiliation,
        role,
        email,
        password,
        status,
        initiator
    ) {
        console.info("============= START : Update Item ===========");

        const txId = await ctx.stub.getTxID();
        const channelId = await ctx.stub.getChannelID();
        const timeStamp = new Date(
            (await ctx.stub.getTxTimestamp().seconds.low) * 1000
        ).toString();

        var queryString = `{"selector":{"email":"${email}"}}`;
        const iterator = await ctx.stub.getQueryResult(queryString);
        const allResults = [];

        const quser = await ctx.stub.getState(email); // get the car from chaincode state
        if (!quser || quser.length === 0) {
            throw new Error(`${email} does not exist`);
        }
        const user = JSON.parse(quser.toString());

        user.name = name;
        user.affiliation = affiliation;
        user.role = role;
        user.password = password;
        user.initiator = initiator;
        user.txId = txId;
        user.channelId = channelId;
        user.dateTime = timeStamp;

        await ctx.stub.putState(email, Buffer.from(JSON.stringify(user)));

        console.info("============= END : Update Item ===========");
    }

    async queryAllUsersByRole(ctx, role) {
        var queryString = `{"selector":{"role":"${role}"}}`;
        const iterator = await ctx.stub.getQueryResult(queryString);

        // const iterator = await ctx.stub.getStateByRange(startKey, endKey);

        const allResults = [];
        while (true) {
            const res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString("utf8"));

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString("utf8"));
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString("utf8");
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log("end of data");
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

    async queryAllUsersByInitiator(ctx, initiator) {
        var queryString = `{"selector":{"initiator":"${initiator}"}}`;
        const iterator = await ctx.stub.getQueryResult(queryString);

        // const iterator = await ctx.stub.getStateByRange(startKey, endKey);

        const allResults = [];
        while (true) {
            const res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString("utf8"));

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString("utf8"));
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString("utf8");
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log("end of data");
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

    async queryUserByEmail(ctx, email) {
        const userAsBytes = await ctx.stub.getState(email); // get the item from chaincode state
        if (!userAsBytes || userAsBytes.length === 0) {
            throw new Error(`${email} does not exist`);
        }
        console.log(userAsBytes.toString());
        return userAsBytes.toString();
    }

    async queryAllUsers(ctx, model) {
        var queryString = `{"selector":{"model":"${model}"}}`;
        const iterator = await ctx.stub.getQueryResult(queryString);

        // const iterator = await ctx.stub.getStateByRange(startKey, endKey);

        const allResults = [];
        while (true) {
            const res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString("utf8"));

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString("utf8"));
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString("utf8");
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log("end of data");
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

    async userHistory(ctx, email) {
        const iterator = await ctx.stub.getHistoryForKey(email);
        return this.getAllResults(ctx, iterator);
    }

    // Requested Item Management
    async createRequest(
        ctx,
        user,
        item,
        quantity,
        state,
        initiator,
        currentState,
        model
    ) {
        console.info("============= START : Create Item ===========");

        const txId = await ctx.stub.getTxID();
        const channelId = await ctx.stub.getChannelID();
        const dateTime = new Date(
            (await ctx.stub.getTxTimestamp().seconds.low) * 1000
        ).toString();

        const id = txId;

        const request = {
            id,
            user,
            item,
            quantity,
            state,
            initiator,
            currentState,
            txId,
            channelId,
            dateTime,
            model
        };

        // this.storeTransaction(ctx);
        await ctx.stub.putState(id, Buffer.from(JSON.stringify(request)));
        console.info("============= END : Create Item ===========");
    }

    async updateExistingRequest(ctx, id, state, initiator, currentState) {
        console.info("============= START : Update Item ===========");

        const txId = await ctx.stub.getTxID();
        const channelId = await ctx.stub.getChannelID();
        const timeStamp = new Date(
            (await ctx.stub.getTxTimestamp().seconds.low) * 1000
        ).toString();

        // var queryString = `{"selector":{"id":"${id}"}}`;
        // const iterator = await ctx.stub.getQueryResult(queryString);
        // const allResults = [];

        const qitem = await ctx.stub.getState(id); // get the car from chaincode state
        if (!qitem || qitem.length === 0) {
            throw new Error(`${itemId} does not exist`);
        }

        const req = JSON.parse(qitem.toString());

        req.state = state;
        req.currentState = currentState;
        req.initiator = initiator;
        req.txId = txId;
        req.channelId = channelId;
        req.dateTime = timeStamp;

        await ctx.stub.putState(id, Buffer.from(JSON.stringify(req)));

        console.info("============= END : Update Item ===========");
    }

    async queryRequestByState(ctx, state) {
        var queryString = `{"selector":{"state":"${state}"}}`;
        const iterator = await ctx.stub.getQueryResult(queryString);

        // const iterator = await ctx.stub.getStateByRange(startKey, endKey);

        const allResults = [];
        while (true) {
            const res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString("utf8"));

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString("utf8"));
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString("utf8");
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log("end of data");
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

    async queryRequestByUser(ctx, user) {
        var queryString = `{"selector":{"user":"${user}"}}`;
        const iterator = await ctx.stub.getQueryResult(queryString);

        // const iterator = await ctx.stub.getStateByRange(startKey, endKey);

        const allResults = [];
        while (true) {
            const res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString("utf8"));

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString("utf8"));
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString("utf8");
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log("end of data");
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

    async queryRequestById(ctx, id) {
        const userAsBytes = await ctx.stub.getState(id); // get the item from chaincode state
        if (!userAsBytes || userAsBytes.length === 0) {
            throw new Error(`${id} does not exist`);
        }
        console.log(userAsBytes.toString());
        return userAsBytes.toString();
    }

    async requestHistory(ctx, id) {
        const iterator = await ctx.stub.getHistoryForKey(id);
        return this.getAllResults(ctx, iterator);
    }

    async queryRequestByModel(ctx, model) {
        var queryString = `{"selector":{"model":"${model}"}}`;
        const iterator = await ctx.stub.getQueryResult(queryString);

        // const iterator = await ctx.stub.getStateByRange(startKey, endKey);

        const allResults = [];
        while (true) {
            const res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString("utf8"));

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString("utf8"));
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString("utf8");
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log("end of data");
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

    // generic function
    async getAllResults(ctx, iterator) {
        let allResults = [];

        while (true) {
            const res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString("utf8"));

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString("utf8"));
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString("utf8");
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log("end of data");
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }
}

module.exports = FabCar;
