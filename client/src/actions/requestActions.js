import axios from "axios";

import {
  GET_ERRORS,
  POST_LOADING,
  GET_REQUEST,
  GET_ALL_REQUESTS,
  SET_CURRENT_REQUEST,
  GET_CURRENT_REQUEST,
  GET_REQUEST_HISTORY
} from "./types";

axios.defaults.baseURL = "http://localhost:5000";

// axios.defaults.headers.common['Access-Control-Allow-Origin'] = "http://localhost:5000";

// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

//Post Item
export const createRequest = (expData, history) => dispatch => {
  axios
    .post("/api/userRequests/request", expData)
    .then(res => history.push("/dashboard"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const updateRequest = (expData, history) => dispatch => {
  axios
    .post("/api/userRequests/request/update", expData)
    .then(res => history.push("/dashboard"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const historyRequest = id => dispatch => {
  axios
    .post(`/api/userRequests/request/history/${id}`)
    .then(res =>
      dispatch({
        type: GET_REQUEST_HISTORY,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_REQUEST_HISTORY,
        payload: null
      })
    );
};

export const getAllRequests = () => dispatch => {
  dispatch(setItemLoading());
  axios
    .get("/api/userRequests/all")
    .then(res =>
      dispatch({
        type: GET_ALL_REQUESTS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ALL_REQUESTS,
        payload: null
      })
    );
};

// Get Item
export const getRequst = id => dispatch => {
  axios
    .get(`/api/userRequests/request/${id}`)
    .then(res => {
      dispatch(setCurrentRequst(res.data));
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Set current item
export const setCurrentRequst = data => {
  return {
    type: SET_CURRENT_REQUEST,
    payload: data
  };
};

// Get current item
export const getCurrentRequst = () => dispatch => {
  dispatch(setItemLoading());
  axios
    .get("/api/userRequests/current")
    .then(res =>
      dispatch({
        type: GET_CURRENT_REQUEST,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_CURRENT_REQUEST,
        payload: {}
      })
    );
};

// Set loading state
export const setItemLoading = () => {
  return {
    type: POST_LOADING
  };
};

export const forwardRequest = id => dispatch => {
  axios
    .post(`/api/userRequests/request/forward/${id}`, {})
    .then(res => window.location.reload())
    .catch(err => window.location.reload());
};

export const cancelRequest = id => dispatch => {
  axios
    .post(`/api/userRequests/request/cancel/${id}`, {})
    .then(res => window.location.reload())
    .catch(err => window.location.reload());
};

export const processedRequest = id => dispatch => {
  axios
    .post(`/api/userRequests/request/processed/${id}`, {})
    .then(res => window.location.reload())
    .catch(err => window.location.reload());
};
