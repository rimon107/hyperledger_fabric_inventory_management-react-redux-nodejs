import axios from "axios";

import {
  GET_ERRORS,
  GET_ALL_USERS,
  POST_LOADING,
  GET_CURRENT_USER,
  GET_USER_HISTORY,
  GET_USER
} from "./types";

axios.defaults.baseURL = "http://localhost:5000";

// axios.defaults.headers.common['Access-Control-Allow-Origin'] = "http://localhost:5000";

// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

//Post Item
export const createUser = (expData, history) => dispatch => {
  axios
    .post("/api/users/user/register", expData)
    .then(res => history.push("/user/list"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const updateUser = (expData, history) => dispatch => {
  axios
    .post("/api/users/user/update", expData)
    .then(res => history.push("/dashboard"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const deleteUser = id => dispatch => {
  axios
    .post(`/api/users/user/delete/${id}`, {})
    .then(res => window.location.reload())
    .catch(err => window.location.reload());
};

export const historyUser = id => dispatch => {
  axios
    .post(`/api/users/user/history/${id}`)
    .then(res =>
      dispatch({
        type: GET_USER_HISTORY,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_USER_HISTORY,
        payload: null
      })
    );
};

// Get All Items
export const getAllUsers = () => dispatch => {
  dispatch(setItemLoading());
  axios
    .get("/api/users/all")
    .then(res =>
      dispatch({
        type: GET_ALL_USERS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ALL_USERS,
        payload: null
      })
    );
};

// Get Item
export const getUser = id => dispatch => {
  axios
    .get(`/api/users/user/${id}`)
    .then(res => {
      dispatch({
        type: GET_USER,
        payload: res.data
      });
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Get current item
export const getCurrentUser = () => dispatch => {
  dispatch(setItemLoading());
  axios
    .get("/api/users/user/current")
    .then(res =>
      dispatch({
        type: GET_CURRENT_USER,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_CURRENT_USER,
        payload: {}
      })
    );
};

// Set loading state
export const setItemLoading = () => {
  return {
    type: POST_LOADING
  };
};
