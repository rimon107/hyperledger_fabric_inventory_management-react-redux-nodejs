import axios from "axios";

import {
  GET_ERRORS,
  GET_ALL_ITEMS,
  POST_LOADING,
  SET_CURRENT_ITEM,
  GET_CURRENT_ITEM,
  GET_ITEM_HISTORY
} from "./types";

axios.defaults.baseURL = "http://localhost:5000";

// axios.defaults.headers.common['Access-Control-Allow-Origin'] = "http://localhost:5000";

// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

//Post Item
export const createItem = (expData, history) => dispatch => {
  console.log("post item");
  axios
    .post("/api/items/item", expData)
    .then(res => history.push("/dashboard"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const updateItem = (expData, history) => dispatch => {
  axios
    .post("/api/items/item/update", expData)
    .then(res => history.push("/dashboard"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const deleteItem = id => dispatch => {
  axios
    .post(`/api/items/item/delete/${id}`, {})
    .then(res => window.location.reload())
    .catch(err => window.location.reload());
};

export const historyItem = id => dispatch => {
  axios
    .post(`/api/items/item/history/${id}`)
    .then(res =>
      dispatch({
        type: GET_ITEM_HISTORY,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ITEM_HISTORY,
        payload: null
      })
    );
};

// Get All Items
export const getAllItems = () => dispatch => {
  dispatch(setItemLoading());
  axios
    .get("/api/items/all")
    .then(res =>
      dispatch({
        type: GET_ALL_ITEMS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ALL_ITEMS,
        payload: null
      })
    );
};

// Get Item
export const getItem = id => dispatch => {
  axios
    .get(`/api/items/item/${id}`)
    .then(res => {
      dispatch(setCurrentItem(res.data));
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Set current item
export const setCurrentItem = data => {
  return {
    type: SET_CURRENT_ITEM,
    payload: data
  };
};

// Get current item
export const getCurrentItem = () => dispatch => {
  dispatch(setItemLoading());
  axios
    .get("/api/items/cur-item")
    .then(res =>
      dispatch({
        type: GET_CURRENT_ITEM,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_CURRENT_ITEM,
        payload: {}
      })
    );
};

// Set loading state
export const setItemLoading = () => {
  return {
    type: POST_LOADING
  };
};
