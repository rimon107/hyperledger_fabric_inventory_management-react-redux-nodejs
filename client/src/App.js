import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import jwt_decode from "jwt-decode";
import setAuthToken from "./utils/setAuthToken";
import { setCurrentUser, logoutUser } from "./actions/authActions";
import { clearCurrentProfile } from "./actions/profileActions";

import { Provider } from "react-redux";
import store from "./store";

import PrivateRoute from "./components/common/PrivateRoute";

// import Navbar from "./components/layout/Navbar";
import Footer from "./components/layout/Footer";
import Landing from "./components/layout/Landing";
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";
import Dashboard from "./components/dashboard/Dashboard";
import CreateProfile from "./components/create-profile/CreateProfile";
import EditProfile from "./components/edit-profile/EditProfile";
import AddExperience from "./components/add-credentials/AddExperience";
import AddEducation from "./components/add-credentials/AddEducation";
import Profiles from "./components/profiles/Profiles";
import Profile from "./components/profile/Profile";
import Posts from "./components/posts/Posts";
import Post from "./components/post/Post";
import NotFound from "./components/not-found/NotFound";

// import "./App.css";

import Item from "./components/item/Item";
import ItemUpdate from "./components/item/UpdateItem";
import ItemHistory from "./components/item/ItemHistory";
import SignIn from "./components/auth/SignIn";
import AuthLayout from "./components/layout/AuthLayout";
import GuestLayout from "./components/layout/GuestLayout";
import AllItemList from "./components/item/AllItemList";
import User from "./components/user/User";
import UserList from "./components/user/AllUserList";
import UserHistoryList from "./components/user/UserHistory";
import UserUpdate from "./components/user/UpdateUser";

import UserRequestCreate from "./components/request/UserRequestCreate";
import UserRequestList from "./components/request/UserRequestList";
import AdminRequestList from "./components/request/AdminRequestList";
import StoreRequestList from "./components/request/StoreRequestList";

// import ItemList from "./components/item/ItemList";

// let login = false;

// Check for token
if (localStorage.jwtToken) {
  // Set auth token header auth
  setAuthToken(localStorage.jwtToken);
  // Decode token and get user info and exp
  const decoded = jwt_decode(localStorage.jwtToken);
  // Set user and isAuthenticated

  store.dispatch(setCurrentUser(decoded));
  // login = true;
  // Check for expired token
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());
    // Clear current Profile
    store.dispatch(clearCurrentProfile());
    // login = false;
    // Redirect to login
    window.location.href = "/login";
  }
}

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <PrivateRoute
              exact
              path="/login"
              component={SignIn}
              layout={GuestLayout}
            />

            <PrivateRoute
              exact
              path="/user/create"
              component={User}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/user/list"
              component={UserList}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/user/update"
              component={UserUpdate}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/user/history"
              component={UserHistoryList}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/profiles"
              component={Profiles}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/profile/:handle"
              component={Profile}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/dashboard"
              component={Dashboard}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/"
              component={Dashboard}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/item/create"
              component={Item}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/item/update"
              component={ItemUpdate}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/item/history"
              component={ItemHistory}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/create-profile"
              component={CreateProfile}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/edit-profile"
              component={EditProfile}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/add-experience"
              component={AddExperience}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/add-education"
              component={AddEducation}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/item/list"
              component={AllItemList}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/request/user/list"
              component={UserRequestList}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/request/user/create"
              component={UserRequestCreate}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/request/admin/list"
              component={AdminRequestList}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/request/store/list"
              component={StoreRequestList}
              layout={AuthLayout}
            />

            <PrivateRoute
              exact
              path="/not-found"
              component={NotFound}
              layout={GuestLayout}
            />
          </Switch>
        </Router>
      </Provider>
    );
  }
}

export default App;
