import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import { clearCurrentProfile } from "../../actions/profileActions";
import appLogo from "./img/AdminLTELogo.png";
import userLogo from "./img/user2-160x160.jpg";
import "./layout.css";

class LeftSidebar extends Component {
  constructor() {
    super();

    const path = window.location.pathname;
    const dashboardCSS =
      "nav-link " + (path === "/dashboard" || path === "/" ? "active" : "");
    const productCSS =
      "nav-item has-treeview " +
      (path === "/item/create" || path === "/item/list"
        ? "active menu-open"
        : "menu-close");
    const productOpenClose = "nav nav-treeview ";
    const productCreateCSS =
      "nav-link " + (path === "/item/create" ? "active" : "");
    const productListCSS =
      "nav-link " + (path === "/item/list" ? "active" : "");

    const userCSS =
      "nav-item has-treeview " +
      (path === "/user/create" || path === "/user/list"
        ? "active menu-open"
        : "menu-close");
    const userOpenClose = "nav nav-treeview ";
    const userCreateCSS =
      "nav-link " + (path === "/user/create" ? "active" : "");
    const userListCSS = "nav-link " + (path === "/user/list" ? "active" : "");

    const requestCSS =
      "nav-item has-treeview " +
      (path === "/request/user/create" ||
      path === "/request/user/list" ||
      path === "/request/admin/create" ||
      path === "/request/store/list"
        ? "active menu-open"
        : "menu-close");
    const requestOpenClose = "nav nav-treeview ";
    const requestUserCreateCSS =
      "nav-link " + (path === "/request/user/create" ? "active" : "");
    const requestUserListCSS =
      "nav-link " + (path === "/request/user/list" ? "active" : "");
    const requestAdminListCSS =
      "nav-link " + (path === "/request/admin/list" ? "active" : "");
    const requestStoreListCSS =
      "nav-link " + (path === "/request/store/list" ? "active" : "");

    this.state = {
      dashboardCSS: dashboardCSS,
      productCSS: productCSS,
      productOpenClose: productOpenClose,
      productCreateCSS: productCreateCSS,
      productListCSS: productListCSS,
      userCSS: userCSS,
      userOpenClose: userOpenClose,
      userCreateCSS: userCreateCSS,
      userListCSS: userListCSS,
      requestCSS: requestCSS,
      requestOpenClose: requestOpenClose,
      requestUserCreateCSS: requestUserCreateCSS,
      requestUserListCSS: requestUserListCSS,
      requestAdminListCSS: requestAdminListCSS,
      requestStoreListCSS: requestStoreListCSS
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    console.log(e);
    console.log(e.currentTarget);
    console.log(e.currentTarget.pathname);
    let path = e.currentTarget.pathname;

    let productOpenClose;
    const dashboardCSS =
      "nav-link " + (path === "/dashboard" || path === "/" ? "active" : "");
    const productCSS =
      "nav-item has-treeview " +
      (path === "/item/create" || path === "/item/list"
        ? "active menu-open"
        : "menu-close");
    const productCreateCSS =
      "nav-link " + (path === "/item/create" ? "active" : "");
    const productListCSS =
      "nav-link " + (path === "/item/list" ? "active" : "");
    productOpenClose =
      "nav nav-treeview " +
      (path === "/item/create" || path === "/item/list"
        ? "open-menu"
        : "close-menu");

    if (e.currentTarget.id === "productOpenClose") {
      // console.log(e.currentTarget.nextSibling);
      // console.log("e.currentTarget.nextSibling.style.display");
      // console.log(e.currentTarget.nextSibling.style.display);
      // console.log(e.currentTarget.nextSibling.style.class);
      // const styleul = e.currentTarget.nextSibling.style.display;

      productOpenClose =
        "nav nav-treeview " +
        (this.state.productOpenClose.indexOf("open-menu") > 0
          ? "close-menu"
          : "open-menu");
    }

    let userOpenClose;
    const userCSS =
      "nav-item has-treeview " +
      (path === "/user/create" || path === "/user/list"
        ? "active menu-open"
        : "menu-close");

    const userCreateCSS =
      "nav-link " + (path === "/user/create" ? "active" : "");
    const userListCSS = "nav-link " + (path === "/user/list" ? "active" : "");
    userOpenClose =
      "nav nav-treeview " +
      (path === "/user/create" || path === "/user/list"
        ? "open-menu"
        : "close-menu");

    if (e.currentTarget.id === "userOpenClose") {
      userOpenClose =
        "nav nav-treeview " +
        (this.state.userOpenClose.indexOf("open-menu") > 0
          ? "close-menu"
          : "open-menu");
    }

    const requestCSS =
      "nav-item has-treeview " +
      (path === "/request/user/create" ||
      path === "/request/user/list" ||
      path === "/request/admin/create" ||
      path === "/request/store/list"
        ? "active menu-open"
        : "menu-close");
    let requestOpenClose = "nav nav-treeview ";
    const requestUserCreateCSS =
      "nav-link " + (path === "/request/user/create" ? "active" : "");
    const requestUserListCSS =
      "nav-link " + (path === "/request/user/list" ? "active" : "");
    const requestAdminListCSS =
      "nav-link " + (path === "/request/admin/list" ? "active" : "");
    const requestStoreListCSS =
      "nav-link " + (path === "/request/store/list" ? "active" : "");

    if (e.currentTarget.id === "requestOpenClose") {
      requestOpenClose =
        "nav nav-treeview " +
        (this.state.userOpenClose.indexOf("open-menu") > 0
          ? "close-menu"
          : "open-menu");
    }

    this.setState({
      dashboardCSS: dashboardCSS,
      productCSS: productCSS,
      productOpenClose: productOpenClose,
      productCreateCSS: productCreateCSS,
      productListCSS: productListCSS,
      userCSS: userCSS,
      userOpenClose: userOpenClose,
      userCreateCSS: userCreateCSS,
      userListCSS: userListCSS,
      requestCSS: requestCSS,
      requestOpenClose: requestOpenClose,
      requestUserCreateCSS: requestUserCreateCSS,
      requestUserListCSS: requestUserListCSS,
      requestAdminListCSS: requestAdminListCSS,
      requestStoreListCSS: requestStoreListCSS
    });
    // console.log(this.state);
  }

  render() {
    console.log(window.location.pathname);
    const { user } = this.props.auth;

    let content;

    const role = user.role;

    if (role === "superadmin") {
      content = (
        <ul
          class="nav nav-pills nav-sidebar flex-column"
          data-widget="treeview"
          role="menu"
          data-accordion="false"
        >
          <li class="nav-item">
            <Link
              onClick={this.onChange}
              className={this.state.dashboardCSS}
              to="/dashboard"
            >
              <i class="nav-icon fas fa-th"></i>
              <p>Dashboard</p>
            </Link>
          </li>
          <li className={this.state.requestCSS}>
            <a
              href="javascript:void(0)"
              onClick={this.onChange}
              id="requestOpenClose"
              class="nav-link "
            >
              <i className="nav-icon fas fa-copy"></i>
              <p>
                Request
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul className={this.state.requestOpenClose}>
              <li class="nav-item">
                <Link
                  onClick={this.onChange}
                  className={this.state.requestUserCreateCSS}
                  to="/request/user/create"
                >
                  <i class="far fa-circle nav-icon"></i>
                  <p>Create</p>
                </Link>
              </li>
              <li class="nav-item">
                <Link
                  onClick={this.onChange}
                  className={this.state.requestUserListCSS}
                  to="/request/user/list"
                >
                  <i class="far fa-circle nav-icon"></i>
                  <p>User Request List</p>
                </Link>
              </li>
              <li class="nav-item">
                <Link
                  onClick={this.onChange}
                  className={this.state.requestAdminListCSS}
                  to="/request/admin/list"
                >
                  <i class="far fa-circle nav-icon"></i>
                  <p>Admin Request List</p>
                </Link>
              </li>
              <li class="nav-item">
                <Link
                  onClick={this.onChange}
                  className={this.state.requestStoreListCSS}
                  to="/request/store/list"
                >
                  <i class="far fa-circle nav-icon"></i>
                  <p>Store Request List</p>
                </Link>
              </li>
            </ul>
          </li>
          <li className={this.state.productCSS}>
            <a
              href="javascript:void(0)"
              onClick={this.onChange}
              id="productOpenClose"
              class="nav-link "
            >
              <i className="nav-icon fas fa-copy"></i>
              <p>
                Product
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul className={this.state.productOpenClose}>
              <li class="nav-item">
                <Link
                  onClick={this.onChange}
                  className={this.state.productCreateCSS}
                  to="/item/create"
                >
                  <i class="far fa-circle nav-icon"></i>
                  <p>Create</p>
                </Link>
              </li>
              <li class="nav-item">
                <Link
                  onClick={this.onChange}
                  className={this.state.productListCSS}
                  to="/item/list"
                >
                  <i class="far fa-circle nav-icon"></i>
                  <p>List</p>
                </Link>
              </li>
            </ul>
          </li>
          <li className={this.state.userCSS}>
            <a
              href="javascript:void(0)"
              id="userOpenClose"
              onClick={this.onChange}
              class="nav-link "
            >
              <i class="nav-icon fas fa-tree"></i>
              <p>
                User Management
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <Link
                  className={this.state.userCreateCSS}
                  onClick={this.onChange}
                  to="/user/create"
                >
                  <i class="far fa-circle nav-icon"></i>
                  <p>User Create</p>
                </Link>
              </li>
              <li class="nav-item">
                <Link
                  className={this.state.userListCSS}
                  onClick={this.onChange}
                  to="/user/list"
                >
                  <i class="far fa-circle nav-icon"></i>
                  <p>User List</p>
                </Link>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <Link to="/contact" class="nav-link">
              <i class="nav-icon fas fa-plus-square"></i>
              <p>
                Contact Us
                {/* <span class="right badge badge-danger">New</span> */}
              </p>
            </Link>
          </li>
          <li class="nav-item">
            <Link to="/about" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                About Us
                {/* <span class="right badge badge-danger">New</span> */}
              </p>
            </Link>
          </li>
        </ul>
      );
    } else if (role === "admin") {
      content = (
        <ul
          class="nav nav-pills nav-sidebar flex-column"
          data-widget="treeview"
          role="menu"
          data-accordion="false"
        >
          <li class="nav-item">
            <Link
              onClick={this.onChange}
              className={this.state.dashboardCSS}
              to="/dashboard"
            >
              <i class="nav-icon fas fa-th"></i>
              <p>Dashboard</p>
            </Link>
          </li>
          <li className={this.state.requestCSS}>
            <a
              href="javascript:void(0)"
              onClick={this.onChange}
              id="requestOpenClose"
              class="nav-link "
            >
              <i className="nav-icon fas fa-copy"></i>
              <p>
                Request
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul className={this.state.requestOpenClose}>
              <li class="nav-item">
                <Link
                  onClick={this.onChange}
                  className={this.state.requestAdminListCSS}
                  to="/request/admin/list"
                >
                  <i class="far fa-circle nav-icon"></i>
                  <p>List</p>
                </Link>
              </li>
            </ul>
          </li>
          <li className={this.state.userCSS}>
            <a
              href=""
              id="userOpenClose"
              onClick={this.onChange}
              class="nav-link "
            >
              <i class="nav-icon fas fa-tree"></i>
              <p>
                User Management
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <Link
                  className={this.state.userCreateCSS}
                  onClick={this.onChange}
                  to="/user/create"
                >
                  <i class="far fa-circle nav-icon"></i>
                  <p>User Create</p>
                </Link>
              </li>
              <li class="nav-item">
                <Link
                  className={this.state.userListCSS}
                  onClick={this.onChange}
                  to="/user/list"
                >
                  <i class="far fa-circle nav-icon"></i>
                  <p>User List</p>
                </Link>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <Link to="/contact" class="nav-link">
              <i class="nav-icon fas fa-plus-square"></i>
              <p>
                Contact Us
                {/* <span class="right badge badge-danger">New</span> */}
              </p>
            </Link>
          </li>
          <li class="nav-item">
            <Link to="/about" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                About Us
                {/* <span class="right badge badge-danger">New</span> */}
              </p>
            </Link>
          </li>
        </ul>
      );
    } else if (role === "store") {
      content = (
        <ul
          class="nav nav-pills nav-sidebar flex-column"
          data-widget="treeview"
          role="menu"
          data-accordion="false"
        >
          <li class="nav-item">
            <Link
              onClick={this.onChange}
              className={this.state.dashboardCSS}
              to="/dashboard"
            >
              <i class="nav-icon fas fa-th"></i>
              <p>Dashboard</p>
            </Link>
          </li>
          <li className={this.state.requestCSS}>
            <a
              href="javascript:void(0)"
              onClick={this.onChange}
              id="requestOpenClose"
              class="nav-link "
            >
              <i className="nav-icon fas fa-copy"></i>
              <p>
                Request
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul className={this.state.requestOpenClose}>
              <li class="nav-item">
                <Link
                  onClick={this.onChange}
                  className={this.state.requestStoreListCSS}
                  to="/request/store/list"
                >
                  <i class="far fa-circle nav-icon"></i>
                  <p>List</p>
                </Link>
              </li>
            </ul>
          </li>
          <li className={this.state.productCSS}>
            <a
              href=""
              onClick={this.onChange}
              id="productOpenClose"
              class="nav-link "
            >
              <i className="nav-icon fas fa-copy"></i>
              <p>
                Product
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul className={this.state.productOpenClose}>
              <li class="nav-item">
                <Link
                  onClick={this.onChange}
                  className={this.state.productCreateCSS}
                  to="/item/create"
                >
                  <i class="far fa-circle nav-icon"></i>
                  <p>Create</p>
                </Link>
              </li>
              <li class="nav-item">
                <Link
                  onClick={this.onChange}
                  className={this.state.productListCSS}
                  to="/item/list"
                >
                  <i class="far fa-circle nav-icon"></i>
                  <p>List</p>
                </Link>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <Link to="/contact" class="nav-link">
              <i class="nav-icon fas fa-plus-square"></i>
              <p>
                Contact Us
                {/* <span class="right badge badge-danger">New</span> */}
              </p>
            </Link>
          </li>
          <li class="nav-item">
            <Link to="/about" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                About Us
                {/* <span class="right badge badge-danger">New</span> */}
              </p>
            </Link>
          </li>
        </ul>
      );
    } else if (role === "officer") {
      content = (
        <ul
          class="nav nav-pills nav-sidebar flex-column"
          data-widget="treeview"
          role="menu"
          data-accordion="false"
        >
          <li class="nav-item">
            <Link
              onClick={this.onChange}
              className={this.state.dashboardCSS}
              to="/dashboard"
            >
              <i class="nav-icon fas fa-th"></i>
              <p>Dashboard</p>
            </Link>
          </li>
          <li className={this.state.requestCSS}>
            <a
              href="javascript:void(0)"
              onClick={this.onChange}
              id="requestOpenClose"
              class="nav-link "
            >
              <i className="nav-icon fas fa-copy"></i>
              <p>
                Request
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul className={this.state.requestOpenClose}>
              <li class="nav-item">
                <Link
                  onClick={this.onChange}
                  className={this.state.requestUserCreateCSS}
                  to="/request/user/create"
                >
                  <i class="far fa-circle nav-icon"></i>
                  <p>Create</p>
                </Link>
              </li>
              <li class="nav-item">
                <Link
                  onClick={this.onChange}
                  className={this.state.requestUserListCSS}
                  to="/request/user/list"
                >
                  <i class="far fa-circle nav-icon"></i>
                  <p>List</p>
                </Link>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <Link to="/contact" class="nav-link">
              <i class="nav-icon fas fa-plus-square"></i>
              <p>
                Contact Us
                {/* <span class="right badge badge-danger">New</span> */}
              </p>
            </Link>
          </li>
          <li class="nav-item">
            <Link to="/about" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                About Us
                {/* <span class="right badge badge-danger">New</span> */}
              </p>
            </Link>
          </li>
        </ul>
      );
    }

    return (
      <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <Link to="/" class="brand-link">
          <img
            src={appLogo}
            alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3"
            Style="opacity: .8"
          />
          <span class="brand-text font-weight-light">Inventory</span>
        </Link>

        <div class="sidebar">
          <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
              <img src={userLogo} class="img-circle elevation-2" alt="User" />
            </div>
            <div class="info">
              <Link to="/" class="d-block">
                {user.name}
              </Link>
            </div>
          </div>

          <nav class="mt-2">{content}</nav>
        </div>
      </aside>
    );
  }
}

LeftSidebar.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { logoutUser, clearCurrentProfile })(
  LeftSidebar
);
