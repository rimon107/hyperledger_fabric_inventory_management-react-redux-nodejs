import React from "react";
import bd from "./img/bd.png";
import ict from "./img/ict.png";

export default () => {
  return (
    <footer class="main-footer">
      <div class="float-right d-none d-sm-inline">
        <strong>Funded By ICTD, BANGLADESH</strong>{" "}
      </div>
      {/* <!-- Default to the left --> */}

      {/* <img src={ict} width="175" height="150" />
      <img src={bd} width="175" height="150" /> */}
    </footer>
  );
};
