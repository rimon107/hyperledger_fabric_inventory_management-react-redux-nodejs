import React from "react";
import Aux from "../hoc/AUX";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import { clearCurrentProfile } from "../../actions/profileActions";

class GuestLayout extends React.Component {
  render() {
    console.log("In Guest");

    return (
      <Aux>
        <div className="login-page">{this.props.children}</div>
      </Aux>
    );
  }
}

GuestLayout.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { logoutUser, clearCurrentProfile })(
  GuestLayout
);
