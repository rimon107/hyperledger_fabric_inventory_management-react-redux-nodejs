import React from "react";
import Aux from "../hoc/AUX";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import { clearCurrentProfile } from "../../actions/profileActions";
import Header from "./Header";
import LSideBar from "./LeftSidebar";
import Footer from "./Footer";

class AuthLayout extends React.Component {
  render() {
    return (
      <Aux>
        <div className="wrapper">
          <Header />
          <LSideBar />

          <div className="content-wrapper">{this.props.children}</div>

          <Footer />
        </div>
      </Aux>
    );
  }
}

AuthLayout.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { logoutUser, clearCurrentProfile })(
  AuthLayout
);
