import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser } from "../../actions/authActions";

class SignIn extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/dashboard");
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
        console.log("this.props");
        console.log(this.props.history);
        this.props.history.push("/dashboard");
    }

    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const userData = {
      email: this.state.email,
      password: this.state.password
    };

    this.props.loginUser(userData);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;

    return (

        <div class="login-box">
            <div class="login-logo">
                <a href="#"><b>Admin</b>LTE</a>
            </div>
            {/* <!-- /.login-logo --> */}
            <div class="card">
                <div class="card-body login-card-body">
                    <p class="login-box-msg">Sign in to start your session</p>

                    <form onSubmit={this.onSubmit}>
                        <div class="input-group mb-3">
                            <input
                              name="email"
                              type="email"
                              value={this.state.email}
                              onChange={this.onChange}
                              error={errors.email}
                              class="form-control" 
                              placeholder="Email" />
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                        </div>
                        <div class="input-group mb-3">
                            <input 
                              name="password"
                              type="password"
                              value={this.state.password}
                              onChange={this.onChange}
                              error={errors.password}
                              class="form-control" 
                              placeholder="Password" />
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-8">
                            <div class="icheck-primary">
                            <input type="checkbox" id="remember" />
                            <label for="remember">
                                Remember Me
                            </label>
                            </div>
                        </div>
                        {/* <!-- /.col --> */}
                        <div class="col-4">
                            <input type="submit" class="btn btn-primary btn-block" value="Sign In" />
                        </div>
                        {/* <!-- /.col --> */}
                        </div>
                    </form>

                

                <p class="mb-1">
                    <a href="forgot-password.html">I forgot my password</a>
                </p>
                {/* <p class="mb-0">
                    <a href="register.html" class="text-center">Register a new membership</a>
                </p> */}
                </div>
                {/* <!-- /.login-card-body --> */}
            </div>
            </div>
    );
  }
}

SignIn.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { loginUser })(SignIn);
