import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Moment from "react-moment";
// import { getIssuePaper } from "../../actions/paperActions";
import { getItem } from "../../actions/itemActions";
import { Link } from "react-router-dom";

class RequestUserList extends Component {
  render() {
    let Items = "";
    Items = this.props.requests.map(req => (
      <tr key={req.id}>
        <td>{req.item}</td>
        <td>{req.quantity}</td>
        <td>{req.state}</td>
        <td>{req.initiator}</td>
        <td>{req.txId}</td>
        <td>{req.channelId}</td>
        <td>
          <Moment format="YYYY/MM/DD">{req.dateTime}</Moment>
        </td>
      </tr>
    ));

    return (
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>
        </div>
        <div class="card-body">
          <table ref="itemList" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Item Name</th>
                <th>Quantity</th>
                <th>State</th>
                <th>Initiator</th>
                <th>Transaction Id</th>
                <th>Channel Id</th>
                <th>Date</th>
              </tr>
              {Items}
            </thead>
          </table>
        </div>
      </div>
    );
  }
}

RequestUserList.propTypes = {
  getRequest: PropTypes.func.isRequired
};

export default connect(null, { getItem })(RequestUserList);
