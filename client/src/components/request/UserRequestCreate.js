import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import TextFieldGroup from "../common/TextFieldGroup";
import TextInputField from "../common/TextInputField";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { createRequest } from "../../actions/requestActions";
import Aux from "../hoc/AUX";

class UserRequest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: "",
      quantity: "",
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const expData = {
      item: this.state.item,
      quantity: this.state.quantity
    };

    this.props.createRequest(expData, this.props.history);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;

    return (
      <Aux>
        <section class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1>Product Create</h1>
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item">
                    <Link to="/">Home</Link>
                  </li>
                  <li class="breadcrumb-item active">Request</li>
                </ol>
              </div>
            </div>
          </div>
        </section>

        <section class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-3"></div>
              <div class="col-md-6">
                <div class="card card-info">
                  <div class="card-header">
                    <h3 class="card-title"></h3>
                  </div>
                  <form onSubmit={this.onSubmit} class="form-horizontal">
                    <div class="card-body">
                      <div class="form-group row">
                        <label for="item" class="col-sm-2 col-form-label">
                          Item
                        </label>
                        <div class="col-sm-10">
                          <TextInputField
                            id="item"
                            placeholder="* Item Name"
                            name="item"
                            value={this.state.item}
                            onChange={this.onChange}
                            error={errors.item}
                          />
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="quantity" class="col-sm-2 col-form-label">
                          Quantity
                        </label>
                        <div class="col-sm-10">
                          <TextInputField
                            id="quantity"
                            placeholder="* Item Quantity"
                            name="quantity"
                            value={this.state.quantity}
                            onChange={this.onChange}
                            error={errors.quantity}
                          />
                        </div>
                      </div>
                    </div>

                    <div class="card-footer">
                      <input
                        type="submit"
                        value="Save"
                        class="btn btn-info float-right"
                      />
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </Aux>
    );
  }
}

UserRequest.propTypes = {
  createRequest: PropTypes.func.isRequired,
  // item: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  request: state.request,
  errors: state.errors
});

export default connect(mapStateToProps, { createRequest })(
  withRouter(UserRequest)
);
