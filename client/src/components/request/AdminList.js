import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Moment from "react-moment";
// import { getIssuePaper } from "../../actions/paperActions";
import { forwardRequest, cancelRequest } from "../../actions/requestActions";
import { Link } from "react-router-dom";
import Aux from "../hoc/AUX";

class RequestAdminList extends Component {
  onForwardClick(id) {
    this.props.forwardRequest(id, this.props.history);
  }

  onCancelClick(id) {
    this.props.cancelRequest(id, this.props.history);
  }

  render() {
    let Items = "";
    Items = this.props.requests.map(req => (
      <tr key={req.id}>
        <td>{req.item}</td>
        <td>{req.quantity}</td>
        <td>{req.state}</td>
        <td>{req.initiator}</td>
        <td>{req.txId}</td>
        <td>{req.channelId}</td>
        <td>
          <Moment format="YYYY/MM/DD">{req.dateTime}</Moment>
        </td>
        {req.state === "request" ? (
          <Aux>
            <td>
              <button
                onClick={this.onForwardClick.bind(this, req.id)}
                className="btn btn-danger"
              >
                FORWARD
              </button>
            </td>
            <td>
              <button
                onClick={this.onCancelClick.bind(this, req.id)}
                className="btn btn-danger"
              >
                CANCEL
              </button>
            </td>
          </Aux>
        ) : (
          ""
        )}
      </tr>
    ));

    return (
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>
        </div>
        <div class="card-body">
          <table ref="itemList" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Item Name</th>
                <th>Quantity</th>
                <th>State</th>
                <th>Initiator</th>
                <th>Transaction Id</th>
                <th>Channel Id</th>
                <th>Date</th>
                <th></th>
                <th></th>
              </tr>
              {Items}
            </thead>
          </table>
        </div>
      </div>
    );
  }
}

RequestAdminList.propTypes = {
  forwardRequest: PropTypes.func.isRequired,
  cancelRequest: PropTypes.func.isRequired
};

export default connect(null, { forwardRequest, cancelRequest })(
  RequestAdminList
);
