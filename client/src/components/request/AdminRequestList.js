import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getCurrentProfile } from "../../actions/profileActions";
// import { getAllPapers } from "../../actions/paperActions";
import { getAllRequests } from "../../actions/requestActions";
import Spinner from "../common/Spinner";
import RequesrAdminList from "../request/AdminList";
import Aux from "../hoc/AUX";

class AdminRequestList extends Component {
  constructor(props) {
    super(props);
    this.props.getAllRequests();
  }

  componentDidMount() {
    this.props.getCurrentProfile();
    this.props.getAllRequests();
  }

  render() {
    const { user } = this.props.auth;
    const { profile, loading } = this.props.profile;
    const { requests } = this.props.request;

    let dashboardContent;
    // let newItems = items.filter();

    if (profile === null || loading) {
      dashboardContent = <Spinner />;
    } else {
      // Check if logged in user has profile data
      if (Object.keys(profile).length > 0) {
        dashboardContent = <RequesrAdminList requests={requests} user={user} />;
      } else {
        // User is logged in but has no profile
        dashboardContent = (
          <div>
            <p className="lead text-muted">Welcome {user.name}</p>
            <p>You have not yet setup a profile, please add some info</p>
            <Link to="/create-profile" className="btn btn-lg btn-info">
              Create Profile
            </Link>
          </div>
        );
      }
    }

    return (
      <Aux>
        <section class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0 text-dark">Admin Request List</h1>
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item">
                    <Link to="/">Home</Link>
                  </li>
                  <li class="breadcrumb-item active">Admin Request List</li>
                </ol>
              </div>
            </div>
          </div>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-12">{dashboardContent}</div>
          </div>
        </section>
      </Aux>
    );
  }
}

AdminRequestList.propTypes = {
  getAllRequests: PropTypes.func.isRequired,
  getCurrentProfile: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
  requests: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  request: state.request,
  auth: state.auth
});

export default connect(mapStateToProps, { getCurrentProfile, getAllRequests })(
  AdminRequestList
);
