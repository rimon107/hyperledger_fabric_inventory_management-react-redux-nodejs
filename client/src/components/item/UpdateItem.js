import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import TextFieldGroup from "../common/TextFieldGroup";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { updateItem, getCurrentItem } from "../../actions/itemActions";
import isEmpty from "../../validation/is-empty";

class UpdateItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: "",
      itemId: "",
      name: "",
      description: "",
      quantity: "",
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    this.props.getCurrentItem();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
    console.log("nextProps.item: " + nextProps.item);
    console.log(nextProps.item);

    if (nextProps.item.item) {
      const upItem = nextProps.item.item;

      // If profile field doesnt exist, make empty string
      upItem.type = !isEmpty(upItem.type) ? upItem.type : "";

      // Set component fields state
      this.setState({
        type: upItem.type,
        itemId: upItem.itemId,
        name: upItem.name,
        description: upItem.description,
        quantity: upItem.quantity
      });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const expData = {
      type: this.state.type,
      itemId: this.state.itemId,
      name: this.state.name,
      description: this.state.description,
      quantity: this.state.quantity,
      currentState: "update"
    };

    this.props.updateItem(expData, this.props.history);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;
    // console.log(this.state);
    // console.log(errors);

    return (
      <div className="add-experience">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              {/* <Link to="/dashboard" className="btn btn-light">
                Go Back
              </Link> */}
              <h1 className="display-4 text-center">Update ITEM</h1>
              <p className="lead text-center">
                Update existing inventory item.
              </p>
              <small className="d-block pb-3">* = required fields</small>
              <form onSubmit={this.onSubmit}>
                <TextFieldGroup
                  placeholder="* Item Type"
                  name="type"
                  value={this.state.type}
                  onChange={this.onChange}
                  error={errors.type}
                  readonly={"readonly"}
                />
                <TextFieldGroup
                  placeholder="* Item Number"
                  name="itemId"
                  value={this.state.itemId}
                  onChange={this.onChange}
                  error={errors.itemId}
                  readonly={"readonly"}
                />
                <TextFieldGroup
                  placeholder="* Item Name"
                  name="name"
                  value={this.state.name}
                  onChange={this.onChange}
                  error={errors.name}
                  readonly={"readonly"}
                />
                <TextFieldGroup
                  placeholder="* Item description"
                  name="description"
                  value={this.state.description}
                  onChange={this.onChange}
                  error={errors.description}
                  readonly={"readonly"}
                />
                <TextFieldGroup
                  placeholder="* Item quantity"
                  name="quantity"
                  value={this.state.quantity}
                  onChange={this.onChange}
                  error={errors.quantity}
                />

                <input
                  type="submit"
                  value="Submit"
                  className="btn btn-info btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

UpdateItem.propTypes = {
  getCurrentItem: PropTypes.func.isRequired,
  updateItem: PropTypes.func.isRequired,
  // item: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  item: state.item,
  errors: state.errors
});

export default connect(mapStateToProps, { updateItem, getCurrentItem })(
  withRouter(UpdateItem)
);
