import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Moment from "react-moment";
// import { getIssuePaper } from "../../actions/paperActions";
import { getItem, deleteItem, historyItem } from "../../actions/itemActions";
import { Link } from "react-router-dom";

class ItemList extends Component {
  onUpdateClick(id) {
    this.props.getItem(id);
  }

  onDeleteClick(id) {
    this.props.deleteItem(id, this.props.history);
  }

  onHistoryClick(id) {
    // this.props.getItem(id);
    this.props.historyItem(id);
  }

  render() {
    let Items = "";
    Items = this.props.items.map(item => (
      <tr key={item.itemId}>
        <td>{item.itemId}</td>
        <td>{item.type}</td>
        <td>{item.name}</td>
        <td>{item.description}</td>
        <td>{item.quantity}</td>
        <td>{item.initiator}</td>
        <td>{item.txId}</td>
        <td>{item.channelId}</td>
        <td>
          <Moment format="YYYY/MM/DD">{item.dateTime}</Moment>
        </td>
        <td>
          <Link to="/item/history">
            <button
              onClick={this.onHistoryClick.bind(this, item.itemId)}
              className="btn btn-danger"
            >
              History
            </button>
          </Link>
        </td>
        <td>
          <Link to="/item/update">
            <button
              onClick={this.onUpdateClick.bind(this, item.itemId)}
              className="btn btn-danger"
            >
              UPDATE
            </button>
          </Link>
        </td>
        <td>
          <button
            onClick={this.onDeleteClick.bind(this, item.itemId)}
            className="btn btn-danger"
          >
            DELETE
          </button>
        </td>
      </tr>
    ));

    return (
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>
        </div>
        <div class="card-body">
          <table ref="itemList" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Item Number</th>
                <th>Type</th>
                <th>Name</th>
                <th>Description</th>
                <th>Quantity</th>
                <th>Initiator</th>
                <th>Transaction Id</th>
                <th>Channel Id</th>
                <th>Date</th>
                <th />
                <th />
                <th />
              </tr>
              {Items}
            </thead>
          </table>
        </div>
      </div>
    );
  }
}

ItemList.propTypes = {
  getItem: PropTypes.func.isRequired
};

export default connect(null, { getItem, deleteItem, historyItem })(ItemList);
