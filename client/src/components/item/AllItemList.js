import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getCurrentProfile } from "../../actions/profileActions";
// import { getAllPapers } from "../../actions/paperActions";
import { getAllItems } from "../../actions/itemActions";
import Spinner from "../common/Spinner";
import ItemList from "../item/ItemList";
import Aux from "../hoc/AUX";

class AllItemList extends Component {
  constructor(props) {
    super(props);
    this.props.getAllItems();
  }

  componentDidMount() {
    // console.log("componentWillMount");
    this.props.getCurrentProfile();
    this.props.getAllItems();
    // console.log(this.props.items);
  }

  render() {
    const { user } = this.props.auth;
    const { profile, loading } = this.props.profile;
    const { items } = this.props.item;

    let dashboardContent;
    // let newItems = items.filter();

    if (profile === null || loading) {
      dashboardContent = <Spinner />;
    } else {
      // Check if logged in user has profile data
      if (Object.keys(profile).length > 0) {
        dashboardContent = <ItemList items={items} user={user} />;
      } else {
        // User is logged in but has no profile
        dashboardContent = (
          <div>
            <p className="lead text-muted">Welcome {user.name}</p>
            <p>You have not yet setup a profile, please add some info</p>
            <Link to="/create-profile" className="btn btn-lg btn-info">
              Create Profile
            </Link>
          </div>
        );
      }
    }

    return (
      <Aux>
        <section class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0 text-dark">Item List</h1>
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item">
                    <Link to="/">Home</Link>
                  </li>
                  <li class="breadcrumb-item active">Item List</li>
                </ol>
              </div>
            </div>
          </div>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-12">{dashboardContent}</div>
          </div>
        </section>
      </Aux>
    );
  }
}

AllItemList.propTypes = {
  getAllItems: PropTypes.func.isRequired,
  getCurrentProfile: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
  items: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  item: state.item,
  auth: state.auth
});

export default connect(mapStateToProps, { getCurrentProfile, getAllItems })(
  AllItemList
);
