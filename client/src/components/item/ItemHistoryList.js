import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Moment from "react-moment";
import { getItem } from "../../actions/itemActions";

class ItemHistoryList extends Component {
  render() {
    let Items = "";
    console.log(this.props.items);
    Items = this.props.items.map(item => (
      <tr key={item.itemId}>
        <td>{item.itemId}</td>
        <td>{item.type}</td>
        <td>{item.name}</td>
        <td>{item.description}</td>
        <td>{item.quantity}</td>
        <td>{item.currentState}</td>
        <td>{item.initiator}</td>
        <td>{item.txId}</td>
        <td>{item.channelId}</td>
        <td>
          <Moment format="YYYY/MM/DD">{item.dateTime}</Moment>
        </td>
      </tr>
    ));

    return (
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>
        </div>
        <div class="card-body">
          <table
            ref="itemHistoryList"
            class="table table-bordered table-striped"
          >
            <thead>
              <tr>
                <th>ID</th>
                <th>Type</th>
                <th>Name</th>
                <th>Description</th>
                <th>Quantity</th>
                <th>State</th>
                <th>Initiator</th>
                <th>Tx</th>
                <th>Channel</th>
                <th>Date</th>
              </tr>
              {Items}
            </thead>
          </table>
        </div>
      </div>
    );
  }
}

ItemHistoryList.propTypes = {
  getItem: PropTypes.func.isRequired
};

export default connect(null, { getItem })(ItemHistoryList);
