import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import TextFieldGroup from "../common/TextFieldGroup";
import TextInputField from "../common/TextInputField";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { createItem } from "../../actions/itemActions";
import Aux from "../hoc/AUX";

class Item extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: "",
      itemId: "",
      name: "",
      description: "",
      quantity: "",
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const expData = {
      type: this.state.type,
      itemId: this.state.itemId,
      name: this.state.name,
      description: this.state.description,
      quantity: this.state.quantity
    };

    this.props.createItem(expData, this.props.history);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;

    return (
      <Aux>
        <section class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1>Product Create</h1>
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item">
                    <Link to="/">Home</Link>
                  </li>
                  <li class="breadcrumb-item active">Product Create</li>
                </ol>
              </div>
            </div>
          </div>
        </section>

        <section class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-3"></div>
              <div class="col-md-6">
                <div class="card card-info">
                  <div class="card-header">
                    <h3 class="card-title"></h3>
                  </div>
                  <form onSubmit={this.onSubmit} class="form-horizontal">
                    <div class="card-body">
                      <div class="form-group row">
                        <label for="itemType" class="col-sm-2 col-form-label">
                          Type
                        </label>
                        <div class="col-sm-10">
                          <TextInputField
                            id="itemType"
                            placeholder="* Item Type"
                            name="type"
                            value={this.state.type}
                            onChange={this.onChange}
                            error={errors.type}
                          />
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="itemId" class="col-sm-2 col-form-label">
                          Number
                        </label>
                        <div class="col-sm-10">
                          <TextInputField
                            id="itemId"
                            placeholder="* Item Number"
                            name="itemId"
                            value={this.state.itemId}
                            onChange={this.onChange}
                            error={errors.itemId}
                          />
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="itemName" class="col-sm-2 col-form-label">
                          Name
                        </label>
                        <div class="col-sm-10">
                          <TextInputField
                            id="itemName"
                            placeholder="* Item Name"
                            name="name"
                            value={this.state.name}
                            onChange={this.onChange}
                            error={errors.name}
                          />
                        </div>
                      </div>
                      <div class="form-group row">
                        <label
                          for="description"
                          class="col-sm-2 col-form-label"
                        >
                          Description
                        </label>
                        <div class="col-sm-10">
                          <TextInputField
                            id="description"
                            placeholder="* Item Description"
                            name="description"
                            value={this.state.description}
                            onChange={this.onChange}
                            error={errors.description}
                          />
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="quantity" class="col-sm-2 col-form-label">
                          Quantity
                        </label>
                        <div class="col-sm-10">
                          <TextInputField
                            id="quantity"
                            placeholder="* Item Quantity"
                            name="quantity"
                            value={this.state.quantity}
                            onChange={this.onChange}
                            error={errors.quantity}
                          />
                        </div>
                      </div>
                    </div>

                    <div class="card-footer">
                      <input
                        type="submit"
                        value="Save"
                        class="btn btn-info float-right"
                      />
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </Aux>

      // <div className="add-experience">
      //   <div className="container">
      //     <div className="row">
      //       <div className="col-md-8 m-auto">
      //         <Link to="/dashboard" className="btn btn-light">
      //           Go Back
      //         </Link>
      //         <h1 className="display-4 text-center">CREATE ITEM</h1>
      //         <p className="lead text-center">Create a new inventory item.</p>
      //         <small className="d-block pb-3">* = required fields</small>
      //         <form onSubmit={this.onSubmit}>
      //           <TextFieldGroup
      //             placeholder="* Item Type"
      //             name="type"
      //             value={this.state.type}
      //             onChange={this.onChange}
      //             error={errors.type}
      //           />
      //           <TextFieldGroup
      //             placeholder="* Item Number"
      //             name="itemId"
      //             value={this.state.itemId}
      //             onChange={this.onChange}
      //             error={errors.itemId}
      //           />
      //           <TextFieldGroup
      //             placeholder="* Item Name"
      //             name="name"
      //             value={this.state.name}
      //             onChange={this.onChange}
      //             error={errors.name}
      //           />
      //           <TextFieldGroup
      //             placeholder="* Item description"
      //             name="description"
      //             value={this.state.description}
      //             onChange={this.onChange}
      //             error={errors.description}
      //           />
      //           <TextFieldGroup
      //             placeholder="* Item quantity"
      //             name="quantity"
      //             value={this.state.quantity}
      //             onChange={this.onChange}
      //             error={errors.quantity}
      //           />

      //           <input
      //             type="submit"
      //             value="Submit"
      //             className="btn btn-info btn-block mt-4"
      //           />
      //         </form>
      //       </div>
      //     </div>
      //   </div>
      // </div>
    );
  }
}

Item.propTypes = {
  createItem: PropTypes.func.isRequired,
  // item: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  item: state.item,
  errors: state.errors
});

export default connect(mapStateToProps, { createItem })(withRouter(Item));
