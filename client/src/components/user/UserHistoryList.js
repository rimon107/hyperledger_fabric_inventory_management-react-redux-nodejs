import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Moment from "react-moment";
import { getUser } from "../../actions/userAction";

class UserHistoryList extends Component {
  render() {
    let Users = "";
    Users = this.props.users.map(user => (
      <tr key={user.email}>
        <td>{user.name}</td>
        <td>{user.affiliation}</td>
        <td>{user.role}</td>
        <td>{user.email}</td>
        <td>{user.password}</td>
        <td>{user.initiator}</td>
        <td>{user.txId}</td>
        <td>{user.channelId}</td>
        <td>
          <Moment format="YYYY/MM/DD">{user.dateTime}</Moment>
        </td>
      </tr>
    ));

    return (
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>
        </div>
        <div class="card-body">
          <table ref="itemList" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Username</th>
                <th>Affiliation</th>
                <th>Role</th>
                <th>Email</th>
                <th>Password</th>
                <th>Initiator</th>
                <th>Transaction Id</th>
                <th>Channel Id</th>
                <th>Date</th>
              </tr>
              {Users}
            </thead>
          </table>
        </div>
      </div>
    );
  }
}

UserHistoryList.propTypes = {
  getUser: PropTypes.func.isRequired
};

export default connect(null, { getUser })(UserHistoryList);
