import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Moment from "react-moment";
// import { getIssuePaper } from "../../actions/paperActions";
import { getUser, deleteUser, historyUser } from "../../actions/userAction";
import { Link } from "react-router-dom";

class UserList extends Component {
  onUpdateClick(id) {
    this.props.getUser(id);
  }

  onDeleteClick(id) {
    this.props.deleteUser(id, this.props.history);
  }

  onHistoryClick(id) {
    // this.props.getItem(id);
    this.props.historyUser(id);
  }

  render() {
    let Users = "";
    Users = this.props.users.map(user => (
      <tr key={user.email}>
        <td>{user.name}</td>
        <td>{user.affiliation}</td>
        <td>{user.role}</td>
        <td>{user.email}</td>
        <td>{user.password}</td>
        <td>{user.initiator}</td>
        <td>{user.txId}</td>
        <td>{user.channelId}</td>
        <td>
          <Moment format="YYYY/MM/DD">{user.dateTime}</Moment>
        </td>
        <td>
          <Link to="/user/history">
            <button
              onClick={this.onHistoryClick.bind(this, user.email)}
              className="btn btn-danger"
            >
              History
            </button>
          </Link>
        </td>
        <td>
          <Link to="/user/update">
            <button
              onClick={this.onUpdateClick.bind(this, user.email)}
              className="btn btn-danger"
            >
              UPDATE
            </button>
          </Link>
        </td>
        <td>
          <button
            onClick={this.onDeleteClick.bind(this, user.email)}
            className="btn btn-danger"
          >
            DELETE
          </button>
        </td>
      </tr>
    ));

    return (
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>
        </div>
        <div class="card-body">
          <table ref="itemList" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Username</th>
                <th>Affiliation</th>
                <th>Role</th>
                <th>Email</th>
                <th>Password</th>
                <th>Initiator</th>
                <th>Transaction Id</th>
                <th>Channel Id</th>
                <th>Date</th>
                <th />
                <th />
                <th />
              </tr>
              {Users}
            </thead>
          </table>
        </div>
      </div>
    );
  }
}

UserList.propTypes = {
  getUser: PropTypes.func.isRequired
};

export default connect(null, { getUser, deleteUser, historyUser })(UserList);
