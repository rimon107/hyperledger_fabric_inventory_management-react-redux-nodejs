import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import TextFieldGroup from "../common/TextFieldGroup";
import TextInputField from "../common/TextInputField";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { updateUser, getCurrentUser } from "../../actions/userAction";
import Aux from "../hoc/AUX";

class UserUpdate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      affiliation: "",
      role: "",
      email: "",
      password: "",
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    this.props.getCurrentUser();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
    console.log(nextProps.user.user);

    if (nextProps.user.user) {
      const upItem = nextProps.user.user;

      // Set component fields state
      this.setState({
        name: upItem.name,
        affiliation: upItem.affiliation,
        role: upItem.role,
        email: upItem.email,
        password: upItem.password
      });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const expData = {
      name: this.state.name,
      affiliation: this.state.affiliation,
      role: this.state.role,
      email: this.state.email,
      password: this.state.password
    };

    this.props.updateUser(expData, this.props.history);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;

    return (
      <Aux>
        <section class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1>User Create</h1>
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item">
                    <Link to="/">Home</Link>
                  </li>
                  <li class="breadcrumb-item active">User Create</li>
                </ol>
              </div>
            </div>
          </div>
        </section>

        <section class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-3"></div>
              <div class="col-md-6">
                <div class="card card-info">
                  <div class="card-header">
                    <h3 class="card-title"></h3>
                  </div>
                  <form onSubmit={this.onSubmit} class="form-horizontal">
                    <div class="card-body">
                      <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">
                          Username
                        </label>
                        <div class="col-sm-10">
                          <TextInputField
                            id="name"
                            placeholder="* User Name"
                            name="name"
                            value={this.state.name}
                            onChange={this.onChange}
                            error={errors.name}
                          />
                        </div>
                      </div>
                      <div class="form-group row">
                        <label
                          for="affiliation"
                          class="col-sm-2 col-form-label"
                        >
                          Affiliation
                        </label>
                        <div class="col-sm-10">
                          <TextInputField
                            id="affiliation"
                            placeholder="* Affiliation"
                            name="affiliation"
                            value={this.state.affiliation}
                            onChange={this.onChange}
                            error={errors.affiliation}
                          />
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="role" class="col-sm-2 col-form-label">
                          Role
                        </label>
                        <div class="col-sm-10">
                          <TextInputField
                            id="role"
                            placeholder="* Item Role"
                            name="role"
                            value={this.state.role}
                            onChange={this.onChange}
                            error={errors.role}
                          />
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">
                          Email
                        </label>
                        <div class="col-sm-10">
                          <TextInputField
                            id="email"
                            placeholder="* Email"
                            name="email"
                            type="email"
                            value={this.state.email}
                            onChange={this.onChange}
                            error={errors.email}
                          />
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="password" class="col-sm-2 col-form-label">
                          Password
                        </label>
                        <div class="col-sm-10">
                          <TextInputField
                            id="password"
                            placeholder="* Password"
                            name="password"
                            type="password"
                            value={this.state.password}
                            onChange={this.onChange}
                            error={errors.password}
                          />
                        </div>
                      </div>
                    </div>

                    <div class="card-footer">
                      <input
                        type="submit"
                        value="Save"
                        class="btn btn-info float-right"
                      />
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </Aux>
    );
  }
}

UserUpdate.propTypes = {
  updateUser: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  user: state.user,
  errors: state.errors
});

export default connect(mapStateToProps, { updateUser, getCurrentUser })(
  withRouter(UserUpdate)
);
