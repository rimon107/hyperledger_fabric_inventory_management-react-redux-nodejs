import {
  GET_REQUEST,
  SET_CURRENT_REQUEST,
  GET_CURRENT_REQUEST,
  GET_REQUEST_HISTORY,
  GET_ALL_REQUESTS
} from "../actions/types";

const initialState = {
  requests: [],
  request: {},
  currentRequest: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ALL_REQUESTS:
      return {
        ...state,
        requests: action.payload,
        currentRequest: false
      };
    case GET_REQUEST_HISTORY:
      return {
        ...state,
        requests: action.payload,
        currentRequest: false
      };
    case GET_REQUEST:
      return {
        ...state,
        requests: action.payload,
        currentRequest: false
      };
    case SET_CURRENT_REQUEST:
      return {
        ...state,
        request: action.payload,
        currentRequest: true
      };
    case GET_CURRENT_REQUEST:
      return {
        ...state,
        request: action.payload,
        currentRequest: false
      };
    default:
      return state;
  }
}
