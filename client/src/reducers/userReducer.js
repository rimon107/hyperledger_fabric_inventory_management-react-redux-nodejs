import {
  GET_ALL_USERS,
  GET_CURRENT_USER,
  GET_USER_HISTORY,
  GET_USER
} from "../actions/types";

const initialState = {
  users: [],
  user: {},
  currentUser: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ALL_USERS:
      return {
        ...state,
        users: action.payload,
        currentUser: false
      };
    case GET_USER:
      return {
        ...state,
        user: action.payload,
        currentUser: false
      };
    case GET_USER_HISTORY:
      return {
        ...state,
        users: action.payload,
        currentUser: false
      };

    case GET_CURRENT_USER:
      return {
        ...state,
        user: action.payload,
        currentUser: true
      };
    default:
      return state;
  }
}
