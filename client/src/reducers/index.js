import { combineReducers } from "redux";
import authReducer from "./authReducer";
import errorReducer from "./errorReducer";
import profileReducer from "./profileReducer";
import postReducer from "./postReducer";
import itemReducer from "./itemReducer";
import userReducer from "./userReducer";
import requestReducer from "./requestReducer";

export default combineReducers({
  auth: authReducer,
  errors: errorReducer,
  profile: profileReducer,
  post: postReducer,
  item: itemReducer,
  user: userReducer,
  request: requestReducer
});
