import {
  GET_ALL_ITEMS,
  GET_ITEM,
  SET_CURRENT_ITEM,
  GET_CURRENT_ITEM,
  GET_ITEM_HISTORY
} from "../actions/types";

const initialState = {
  items: [],
  item: {},
  currentItem: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ALL_ITEMS:
      return {
        ...state,
        items: action.payload,
        currentItem: false
      };
    case GET_ITEM_HISTORY:
      return {
        ...state,
        items: action.payload,
        currentItem: false
      };
    case GET_ITEM:
      return {
        ...state,
        items: action.payload,
        currentItem: false
      };
    case SET_CURRENT_ITEM:
      return {
        ...state,
        item: action.payload,
        currentItem: true
      };
    case GET_CURRENT_ITEM:
      return {
        ...state,
        item: action.payload,
        currentItem: true
      };
    default:
      return state;
  }
}
